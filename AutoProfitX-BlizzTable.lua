------------------------------------------------------------------------------------------
-- AutoProfitX-Table - r1
--
-- Ace3 config code for Blizzard Option Table
--
-- Send suggestions, comments, and bugs to bug@kanadian.net.
------------------------------------------------------------------------------------------

local AddOnName = "AutoProfitX";
local DBName = "AutoProfitXDB";

-- Create our addon.
AutoProfitX = LibStub( "AceAddon-3.0" ):NewAddon( AddOnName, 
                                              "AceConsole-3.0", 
                                              "AceEvent-3.0" );

local L = LibStub( "AceLocale-3.0" ):GetLocale( AddOnName, false );

local Config = {
     Spinner = {
          { name = L["APX_MISC_NEVER"], value = 0, },
          { name = L["APX_MISC_MOUSEOVER_PROFIT"], value = 1, },
          { name = L["APX_MISC_MOUSEOVER"], value = 2, },
          { name = L["APX_MISC_PROFIT"], value = 3, },
     },
     colorType = {
          { name = L["APX_COLORLESS"], value = 1, },
          { name = L["APX_COLORED"], value = 2, },
     },
     printType = {
          { name = L["APX_TEXT"], value = 1, },
          { name = L["APX_COIN"], value = 2, },
     },
}

_G["apxArray"] = {
     Author = {
          ["Lindarena@Laughing Skull"] = true,
          ["Nagdand@Laughing Skull"] = true,
          ["Nagdand@Korialstrasz"] = true,
          ["Holytree@Laughing Skull"] = true,
          ["Loansone@Laughing Skull"] = true,
          ["Koton@Laughing Skull"] = true,
     },
     sCmds = {
          "autoprofitx",
          "autoprofit",
          "apx",
     },
     Genders = {
          "Unknown",
          "MALE",
          "FEMALE",
     },
     apx = {
          type = "group",
          name = L["APX_OPTIONS_ADDON_LABEL"],
          args = {
               confgendesc = {
                    type = "description", order = 1,
                    name = L["APX_ABOUT_ABOUT_DESC"],
                    cmdHidden = true,
               },
               cat1 = {
                    type = "group", order = 2, inline = true,
                    name = L["APX_ABOUT_ABOUT"],
                    args = {
                         confversiondesc = {
                              type = "description", order = 2,
                              name = L["APX_ABOUT_VERSION"]:format( GetAddOnMetadata( AddOnName, "Version" ) ),
                              cmdHidden = true,
                         },
                         confauthordesc = {
                              type = "description", order = 3,
                              name = L["APX_ABOUT_AUTHOR"]:format( GetAddOnMetadata( AddOnName, "Author" ) ),
                              cmdHidden = true,
                         },
                         confcatdesc = {
                              type = "description", order = 4,
                              name = L["APX_ABOUT_CATEGORY"]:format( GetAddOnMetadata( AddOnName, "X-Category" ) ),
                              cmdHidden = true,
                         },
                         confemaildesc = {
                              type = "description", order = 5,
                              name = L["APX_ABOUT_EMAIL"]:format( GetAddOnMetadata( AddOnName, "X-Email" ) ),
                              cmdHidden = true,
                         },
                         confwebsitedesc = {
                              type = "description", order = 6,
                              name = L["APX_ABOUT_WEBSITE"]:format( GetAddOnMetadata( AddOnName, "X-Website" ) ),
                              cmdHidden = true,
                         },
                         confmemorydesc = {
                              type = "description", order = 7,
                              name = function()
                                   collectgarbage( "collect" );
                                   UpdateAddOnMemoryUsage();
                                   mem = floor( GetAddOnMemoryUsage( AddOnName ) );
                                   return L["APX_ABOUT_MEMORY"]:format( mem );
                              end,
                              cmdHidden = true,
                         },
                    },
               },
               cat2 = {
                    type = "group", order = 3,
                    name = L["APX_OPTIONS_LABEL_ADDON"],
                    args = {
                         loadmsg = {
                              type = "toggle", order = 7,
                              name = L["APX_ABOUT_LOAD"],
                              desc = L["APX_ABOUT_LOAD_DESC"],
                              get = function() return AutoProfitX.db.global.showGreeting; end,
                              set = function( _, value )
                                   AutoProfitX.db.global.showGreeting = value; 
                              end,
                         },
                         optdebug = {
                              type = "toggle", order = 8,
                              name = L["APX_DEBUG_OPTDEBUG"],
                              desc = L["APX_DEBUG_OPTDEBUG_DESC"],
                              hidden = function()
                                   return not _G["apxArray"].Author[ GetUnitName( "player" ) .. "@" .. GetRealmName() ]; -- ); then
                              end,
                              get = function() return AutoProfitX.db.char.debug.state; end,
                              set = function( _, value )
                                   AutoProfitX.db.char.debug.state = value; 
                              end,
                         },
                    },
               },
               cat3 = {
                    type = "group", order = 4,
                    name = L["APX_OPTIONS_SETTINGS_LABEL"],
                    args = {
                         autoSell = {
                              type = "toggle", order = 1,
                              name = L["APX_OPTIONS_AUTOSELL"],
                              desc = L["APX_OPTIONS_AUTOSELL_DESC"],
                              get = function () return AutoProfitX.db.char.autoSell;  end,
                              set = function( _, value )
                                   AutoProfitX.db.char.autoSell = value;
                                   if ( MerchantFrame:IsVisible() and MerchantFrame.selectedTab == 1 ) then 
                                        AutoProfitX:OnShowButton();
                                   end
                              end,
                         },
                         openBags = {
                              type = "toggle", order = 2,
                              name = L["APX_OPTIONS_BAG"],
                              desc = L["APX_OPTIONS_BAG_DESC"],
                              get = function () return AutoProfitX.db.char.openBags; end,
                              disabled = function()
                                   local bagCheck = AutoProfitX:otherBag()
                                   return bagCheck;
                              end,
                              set = function ( _, value )
                                   AutoProfitX.db.char.openBags = value;
                              end,
                         },
                         sellSoulbound = {
                              type = "toggle", order = 3,
                              name = L["APX_OPTIONS_SOULBOUND"],
                              desc = L["APX_OPTIONS_SOULBOUND_DESC"],
                              get = function () return AutoProfitX.db.char.checkSoulbound; end,
                              set = function ( _, value )
                                   if ( value ) then
                                        AutoProfitX:ConcentSellSoulbound( value );
                                   else
                                        AutoProfitX.db.char.checkSoulbound = value;
                                        AutoProfitX:ButtonState();
                                   end
                              end,
                         },

                         sellarmorsub = {
                              type = "toggle", order = 4,
                              name = L["APX_OPTIONS_SELLARMORSUB"],
                              desc = function()
                                   local class = AutoProfitX:GetLocalClass();
                                   local ArmorSub = AutoProfitX_SubArmor[ class ];
                                   local formated = ArmorSub:format( class );
                                   local formated = AutoProfitX:Phase( formated )
	                           return L["APX_OPTIONS_SELLARMORSUB_DESC"]:format( formated );
                              end,
                              disabled = function()
                                   if ( UnitLevel( "player" ) < 40 ) then
                                        return true;
                                   else
                                        return false;
                                   end
                              end,
                              get = function() return AutoProfitX.db.char.sellarmorsub; end,
                              set = function( _, value )
                                   AutoProfitX.db.char.sellarmorsub = value;
                                   AutoProfitX:ButtonState();
                              end
                         },
                         sales = {
                              type = "group", order = 5, inline = true,
                              name = L["APX_OPTIONS_PRINT_LABEL"],
          	              args = {
                                   profit = {
                                        type = "toggle", order = 1,
                                        name = L["APX_OPTIONS_PROFIT"],
                                        desc = L["APX_OPTIONS_PROFIT_DESC"],
                                        get = function () return AutoProfitX.db.char.showTotal; end,
                                        set = function ( _, value )
                                             AutoProfitX.db.char.showTotal = value;
                                        end,
                                   },
                                   silent = {
                                        type = "toggle", order = 2,
                                        name = L["APX_OPTIONS_SALES"],
                                        desc = L["APX_OPTIONS_SALES_DESC"],
                                        get = function () return AutoProfitX.db.char.silent; end,
                                        set = function ( _, value )
                                             AutoProfitX.db.char.silent = value;
                                        end,
                                   },

                                   fancysales = {
                                        type = "toggle", order = 3,
                                        name = L["APX_OPTIONS_FANCY"],
                                        desc = L["APX_OPTIONS_FANCY_DESC"],
                                        get = function () return AutoProfitX.db.char.fancysales; end,
                                        set = function ( _, value )
                                             AutoProfitX.db.char.fancysales = value;
                                        end,
                                   },

                                   stackcount = {
                                        type = "toggle", order = 3,
                                        name = L["APX_OPTIONS_STACK"],
                                        desc = L["APX_OPTIONS_STACK_DESC"],
                                        get = function () return AutoProfitX.db.char.stackCount; end,
                                        set = function ( _, value )
                                             AutoProfitX.db.char.stackCount = value;
                                        end,
                                   },
                                   profits = {
                                        type = "select", order = 4,
                                        name = L["APX_OPTIONS_FANCY_PROFIT"],
                                        desc = L["APX_OPTIONS_FANCY_PROFIT_DESC"],
                                        get = function() return AutoProfitX.db.char.profits; end,
                                        set = function( _, value ) AutoProfitX.db.char.profits = value; end,
                                        values = function()
                                             local preList = {};
                                             local v;
                                             for _, v in pairs( Config.printType ) do
                                                  if ( v.value ~= AutoProfitX.db.char.profits ) then
                                                       preList[v.value] = v.name
                                                  else
                                                       preList[v.value] = ( "|cffffff9a%s|r" ):format( v.name );
                                                  end
                                             end
                                             return preList;
                                        end,
                                   },
                                   colorprofits = {
                                        type = "select", order = 5,
                                        name = L["APX_OPTIONS_COLOR_TEXT"],
                                        desc = L["APX_OPTIONS_COLOR_TEXT_DESC"],
                                        get = function() return AutoProfitX.db.char.cText; end,
                                        set = function( _, value ) AutoProfitX.db.char.cText = value; end,
                                        values = function()
                                             local preList = {};
                                             local v;
                                             for _, v in pairs( Config.colorType ) do
                                                  if ( v.value ~= AutoProfitX.db.char.cText ) then
                                                       preList[v.value] = v.name
                                                  else
                                                       preList[v.value] = "|cffffff9a"..v.name.."|r"
                                                  end
                                             end
                                             return preList;
                                        end,
                                   },
                              },
                         },
                         button = {
                              type = "group", order = 6, inline = true,
                              name = L["APX_BUTTON_SETTINGS"],
                              args = {
                                   buttonSpinRate = {
                                        type = "range", order = 1,
                                        name = L["APX_OPTIONS_GOLDPILE"],
                                        desc = L["APX_OPTIONS_GOLDPILE_DESC"],
                                        min = 1,
                                        max = 360,
                                        step = 5,
                                        get = function() return AutoProfitX.db.char.spinRate; end,
                                        set = function( _, value )
                                             AutoProfitX_SellTab_Button_TreasureModel.rotRate = value;
                                             AutoProfitX.db.char.spinRate = value;
                                        end,
                                   },
                                   buttonSpin = {
                                        type = "select", order = 2,
                                        name = L["APX_OPTIONS_BUTTONA"],
                                        desc = L["APX_OPTIONS_BUTTONA_DESC"],
                                        get = function() return AutoProfitX.db.char.buttonSpin; end,
                                        set = function( _, value ) AutoProfitX.db.char.buttonSpin = value; end,
                                        values = function()
                                             local preList = {};
                                             local v;
                                             for _, v in pairs( Config.Spinner ) do
                                                  if ( v.value ~= AutoProfitX.db.char.buttonSpin ) then
                                                       preList[v.value] = v.name
                                                  else
                                                       preList[v.value] = "|cffffff9a"..v.name.."|r"
                                                  end
                                             end
                                             return preList;
                                        end,
                                   },
                              },
                         },
                    },
               },
               cat4 = {
                    type = "group", order = 5,
                    name = L["APX_ABOUT_MINIMAP_ICON"],
                    args = {
                         confgendesc = {
                              type = "description", order = 1,
                              name = L["APX_ABOUT_LDB_DESC"],
                              cmdHidden = true,
                         },
                         hidemini = {
                              type = "toggle", order = 2,
                              name = L["APX_ABOUT_MINIMAP"],
                              desc = L["APX_ABOUT_MINIMAP_DESC"],
                              get = function() return AutoProfitX.db.global.apxb.hide; end,
                              set = function( _, value )
                                   if ( not value ) then
                                        APXLDBIcon:Show( AddOnName );
                                   else
                                        APXLDBIcon:Hide( AddOnName );
                                   end
                                   AutoProfitX.db.global.apxb.hide = value; 
                              end,
                         },
                    },
               },
               cat5 = {
                    type = "group", order = 6,
                    name = L["APX_DEBUG_LABEL"],
                    hidden = function() return not AutoProfitX.db.char.debug.state; end,
                    args = {
                         confinfodesc = {
                              type = "group", order = 1, inline = true,
                              name = L["APX_DEBUG_SETTINGS"],
                              args = {
                                   confgendesc = {
                                        type = "description", order = 1,
                                        name = L["APX_DEBUG_DESC"],
                                        cmdHidden = true,
                                   },
                              },
                         },
                    },
               },
          },
     },
     sOpts = {
          name = L["APX_SLASH_LABEL"],
          type = "group",
          handler = AutoProfitX,
          args = {
               [ L["APX_SLASH_OPTIONS"] ] = {
                    type = "execute", order = 1,
                    name = L["APX_SLASH_OPTIONS"],
                    desc = L["APX_SLASH_OPTIONS_DESC"],
                    func = function()
                       InterfaceOptionsFrame_OpenToCategory( AutoProfitX.ConfigFrames.AutoProfitX );
                    end,
               },
               [ L["APX_SLASH_TOOLS"] ] = {
                    type = "execute", order = 2,
                    name = L["APX_SLASH_TOOLS"],
                    desc = L["APX_SLASH_TOOLS_DESC"]:format( L["APX_FRAME_EXCEPTIONS"], L["APX_FRAME_EXEMPTIONS"], L["APX_SLASH_TOOLS"] ),
                    func = function()
                         if ( not AutoProfitXFrame ) then
                              AutoProfitX:MakeFrame();
                         else
                              AutoProfitXFrame:Hide();
                              AutoProfitXFrame = nil;
                         end
                    end,
               },
               [ L["APX_SLASH_LIST"] ] = {
                    type = "execute", order = 3,
                    name = L["APX_SLASH_LIST"],
                    desc = L["APX_SLASH_LIST_DESC"]:format( L["APX_FRAME_EXCEPTIONS"], L["APX_FRAME_EXEMPTIONS"] ),
                    func = function( self )
                         local _, v2 = string.match( self.input, "(.*)%s(.*)" );
                         if ( not v2 ) then 
                              AutoProfitX:Print( "error", L["APX_SLASH_LIST_DESC"] );
                              return;
                         end
                         AutoProfitX:ListExceptions( v2 );
                    end
               },
               [ L["APX_SLASH_ADD_EXCEPTIONS"] ] = {
                    type = "execute", order = 4,
                    name = L["APX_SLASH_ADD"],
                    desc = L["APX_SLASH_ADD_DESC"]:format( L["APX_SLASH_ADD_EXCEPTIONS"] ),
                    func = function( self )
                         local str = self.input;
                         local toRemove = L["APX_SLASH_ADD_EXCEPTIONS"] .. " "
                         local sgsub = string.gsub( str, toRemove, "" );
                         if ( not sgsub ) then
                              AutoProfitX:Print( "error", L["APX_SLASH_ADD_DESC"]:format( L["APX_SLASH_ADD_EXCEPTIONS"] ) );
                              return;
                         end

                         AutoProfitX:idUtils( "idAdd", "Exceptions", true, sgsub );
                    end,
               },
               [ L["APX_SLASH_REMOVE_EXCEPTIONS"] ] = {
                    type = "execute", order = 5,
                    name = L["APX_SLASH_REMOVE"],
                    desc = L["APX_SLASH_REMOVE_DESC"]:format( L["APX_SLASH_REMOVE_EXCEPTIONS"] ),
                    func = function( self )
                         local str = self.input;
                         local toRemove = L["APX_SLASH_REMOVE_EXCEPTIONS"] .. " "
                         local sgsub = string.gsub( str, toRemove, "" );
                         if ( not sgsub ) then
                              AutoProfitX:Print( "error", L["APX_SLASH_REMOVE_DESC"]:format( L["APX_SLASH_REMOVE_EXCEPTIONS"] ) );
                              return;
                         end
                         AutoProfitX:idUtils( "idRemove", "Exceptions", true, sgsub );
                    end,
               },
               [ L["APX_SLASH_ADD_EXEMPTIONS"] ] = {
                    type = "execute", order = 6,
                    name = L["APX_SLASH_ADD"],
                    desc = L["APX_SLASH_ADD_DESC"]:format( L["APX_SLASH_ADD_EXEMPTIONS"] ),
                    func = function( self )
                         local str = self.input;
                         local toRemove = L["APX_SLASH_ADD_EXEMPTIONS"] .. " "
                         local sgsub = string.gsub( str, toRemove, "" );
                         if ( not sgsub ) then
                              AutoProfitX:Print( "error", L["APX_SLASH_ADD_DESC"]:format( L["APX_SLASH_ADD_EXEMPTIONS"] ) );
                              return;
                         end
                         print( "passed error reporting" )
                         AutoProfitX:idUtils( "idAdd", "Exemptions", true, sgsub );
                    end,
               },
               [ L["APX_SLASH_REMOVE_EXEMPTIONS"] ] = {
                    type = "execute", order = 7,
                    name = L["APX_SLASH_REMOVE"],
                    desc = L["APX_SLASH_REMOVE_DESC"]:format( L["APX_SLASH_REMOVE_EXEMPTIONS"] ),
                    func = function( self )
                         local str = self.input;
                         local toRemove = L["APX_SLASH_REMOVE_EXEMPTIONS"] .. " "
                         local sgsub = string.gsub( str, toRemove, "" );
                         if ( not sgsub ) then
                              AutoProfitX:Print( "error", L["APX_SLASH_REMOVE_DESC"] );
                              return;
                         end
                         AutoProfitX:idUtils( "idRemove", "Exemptions", true, sgsub );
                    end,
               },
          },
     },
}