------------------------------------------------------------------------------------------
-- AutoProfit - r80
--
-- Automatically sell junk items when you click the button on the vendors window.
--
-- Send suggestions, comments, and bugs to bug@kanadian.net.
------------------------------------------------------------------------------------------

local AddOnName = "AutoProfitX";
local DBName = "AutoProfitXDB";
local Version = GetAddOnMetadata( AddOnName, "Version" );
local scanTip; -- , AutoProfitXFrame;

local L = LibStub( "AceLocale-3.0" ):GetLocale( AddOnName, false );

local LDB = LibStub( "LibDataBroker-1.1", true );
APXLDBIcon = LDB and LibStub( "LibDBIcon-1.0", true );

if ( not scanTip ) then -- :GetName() ~= "AutoProfitXScanTip" ) then
     scanTip = CreateFrame( "GameTooltip", "AutoProfitXScanTip", UIParent, "GameTooltipTemplate" );
     scanTip:SetOwner( UIParent, "ANCHOR_NONE" );
end

--make proficiency table local (defined in proficiencies.lua)
local prof = AutoProfitX_Proficiencies[ select( 2, UnitClass( "player" ) ) ] or {};
AutoProfitX_Proficiencies = nil;

--some strings commonly used in addon
local linkMatch = L["APX_FORMATED_LM"];
local classMatch = string.format( L["APX_FORMATED_ICA"], "([%w, ]*)" );

local apxDefaults = {
     global = {
          Exemptions = {
               Global = {},
          },
          Exceptions = {
               Global = {},
          },
          Temp = {},
          showGreeting = true,
          apxb = {
               hide = false,
          },
          bag = {
               [0] = false,
               [1] = false,
               [2] = false,
               [3] = false,
               [4] = false,
          },
          otherBag = false,
          Version = false,
          frameOpen = false,
          regx = "[a-zA-Z]%s\|(.*)",
          openFrame = "Exceptions",
     },
     char = {
          debug = {
               state = false,
               temp = {};
               sList = {};
          },
          autoSell = false,
          silent = false,
          fancysals = false,
          showTotal = true,
          checkSoulbound = false,
          sellsubarmor = false,
          buttonSpin = 3,
          spinRate = 25,
          profits = 1,
          stackCount = false,
          openBags = false,
          cText = 1,
     },
}

-- AutoProfitX:OnInitialize()
-- =========================================================
function AutoProfitX:OnInitialize()
     -- Set up our database
     self.db = LibStub( "AceDB-3.0" ):New( DBName, apxDefaults, true );

     -- Set up our config options.
     local AutoProfitXConf = LibStub( "AceConfig-3.0" );
     local AutoProfitXReg  = LibStub( "AceConfigRegistry-3.0" );
     local AutoProfitXDiag = LibStub( "AceConfigDialog-3.0" );

     AutoProfitXConf:RegisterOptionsTable( AddOnName, _G["apxArray"].sOpts, _G["apxArray"].sCmds );

     AutoProfitXReg:RegisterOptionsTable( AddOnName .. " apx", _G["apxArray"].apx );

     self.ConfigFrames = { AutoProfitX = AutoProfitXDiag:AddToBlizOptions( AddOnName .. " apx", AddOnName ), }

     self:RegisterEvent("MERCHANT_SHOW");
     self:RegisterEvent("MERCHANT_CLOSED");
     self:RegisterEvent("BAG_UPDATE");
     self:RegisterEvent("PLAYER_ENTERING_WORLD");

     if ( not AutoProfitX.db.global.Version or AutoProfitX.db.global.Version == false or AutoProfitX.db.global.Version ~= GetAddOnMetadata( AddOnName, "Version" ) ) then
	  AutoProfitX.db:ResetDB( Default );
          if ( AutoProfitX.db.global.Version == false ) then AutoProfitX.db.global.Version = "r1"; end
          AutoProfitX:ReSetDB( L["APX_OPTIONS_ADDON_LABEL"], GetAddOnMetadata( AddOnName, "Version" ), AutoProfitX.db.global.Version )
     end

     AutoProfitX.db.global.frameOpen = false;
     -- self.db.char.debug.temp = apxcc;

     -- LDB launcher
     -- ====================
     if ( LDB ) then
          AutoProfitXLauncher = LDB:NewDataObject( "AutoProfitX", {
               type = "launcher",
               icon = "Interface\\Icons\\INV_Misc_Bag_EnchantedMageweave",
               OnClick = function( clickedframe, button )
                    if ( button == "RightButton" ) then
                         ToggleDropDownMenu( 1, nil, AutoProfitXDropDownMenu, clickedframe:GetName(), 0, -5 )
                         -- AutoProfitX:ButtonClick( "config" );
                    else
                         AutoProfitX:ButtonClick( "list" );
                    end
               end,
               OnTooltipShow = function( tt, fancy )
                    tt:AddLine( L["APX_MINIMAP_TEXT1"]:format( Version ) );
                    tt:AddLine( L["APX_MINIMAP_TEXT2"] );
                    tt:AddLine( L["APX_MINIMAP_TEXT3"] );
                    local profit = AutoProfitX:GetProfit( 1 );
                    tt:AddLine( L["APX_MINIMAP_TEXT4"]:format( AutoProfitX:GetMoneyString( profit, AutoProfitX.db.char.profits ) ) );
               end,
          } )
          if ( APXLDBIcon and not IsAddOnLoaded( "Broker2FuBar" ) and not IsAddOnLoaded( "FuBar" ) ) then
               APXLDBIcon:Register( "AutoProfitX", AutoProfitXLauncher, AutoProfitX.db.global.apxb );
          end
     end
end

-- AutoProftX:GetLocalClass()
-- =========================================================
function AutoProfitX:GetLocalClass()
     return select( 2, UnitClass( "player" ) );
end

-- AutoProftX:DropDownMenuOnLoad()
-- =========================================================
function AutoProfitX:DropDownMenuOnLoad()
     UIDropDownMenu_Initialize( AutoProfitXDropDownMenu, DropDownMenu_Initialize, "MENU", 1 );
end

-- DropDownMenu_Initialize( frame, level )
-- =========================================================
function DropDownMenu_Initialize( frame, level )
     local apxInfo
     apxInfo = UIDropDownMenu_CreateInfo();
     apxInfo["text"] = AddOnName .. " " .. L["APX_OPTIONS_LABEL"];
     apxInfo["isTitle"] = true;
     apxInfo["notCheckable"] = true;
     UIDropDownMenu_AddButton( apxInfo );

     apxInfo = UIDropDownMenu_CreateInfo();
     apxInfo["text"] = "";
     apxInfo["isTitle"] = true;
     apxInfo["notCheckable"] = true;
     UIDropDownMenu_AddButton( apxInfo );

     apxInfo = UIDropDownMenu_CreateInfo();
     apxInfo["text"] = L["APX_OPTIONS_LABEL"];
     apxInfo["checked"] = nil;
     apxInfo["func"] = function() AutoProfitX:ButtonClick( "config" ); end;
     apxInfo["icon"] = nil;
     apxInfo["arg1"] = "" .. 1;
     apxInfo["tCoordLeft"] = 0;
     apxInfo["tCoordRight"] = 1;
     apxInfo["tCoordTop"] = 0;
     apxInfo["tCoordBottom"] = 1;
     apxInfo["notCheckable"] = true;
     UIDropDownMenu_AddButton( apxInfo );
end

-- AutoProfitX:OnEnable()
-- =========================================================
function AutoProfitX:OnEnable()
     self:RegisterEvent("MERCHANT_SHOW");
     self:RegisterEvent("MERCHANT_CLOSED");
     self:RegisterEvent("BAG_UPDATE");
     self:RegisterEvent("PLAYER_ENTERING_WORLD");
end

-- AutoProfitX:OnDisable()
-- =========================================================
function AutoProfitX:OnDisable()
     self:UnregisterEvent("MERCHANT_SHOW");
     self:UnregisterEvent("MERCHANT_CLOSED");
     self:UnregisterEvent("BAG_UPDATE");
     self:UnregisterEvent("PLAYER_ENTERING_WORLD");
end

-- AutoProfitX:BAG_UPDATE()
-- =========================================================
function AutoProfitX:BAG_UPDATE()
     AutoProfitX:ButtonState();
end

-- AutoProfitX:PLAYER_ENTERING_WORLD()
-- =========================================================
function AutoProfitX:PLAYER_ENTERING_WORLD()
     local bagCheck = AutoProfitX:otherBag();
     if ( bagCheck ) then AutoProfitX.db.char.openBags = false; end
     if ( AutoProfitX.db.global.showGreeting ) then
          collectgarbage( "collect" );
          UpdateAddOnMemoryUsage();
          local memFormated = ( "%s kb" ):format( floor( GetAddOnMemoryUsage( "AutoProfitX" ) ) );
          AutoProfitX:Print( "print", L["APX_BANNER_FORMAT"]:format( Version, memFormated ) );
     end
end

-- AutoProfitX:MERCHANT_SHOW()
-- =========================================================
function AutoProfitX:MERCHANT_SHOW()
     if ( AutoProfitX.db.char.openBags ) then
          AutoProfitX:OpenAllBags( false );
     end
     if ( AutoProfitX.db.char.autoSell ) then
          local profit = AutoProfitX:GetProfit( 0 );
          if ( profit > 0 ) then
               AutoProfitX:SellJunk();
               if ( AutoProfitX.db.char.showTotal ) then
                    AutoProfitX:Print( "print", format( L["APX_FORMATED_PROFITS"], AutoProfitX:GetMoneyString( profit, AutoProfitX.db.char.profits ) ) );
               end
          end
     else
          AutoProfitX_SellTab:Show();
     end
end

-- AutoProfitX:MERCHANT_CLOSED()
-- =========================================================
function AutoProfitX:MERCHANT_CLOSED()
     if ( AutoProfitX.db.char.openBags ) then
          AutoProfitX:OpenAllBags( true );
     end
     if ( AutoProfitX_SellTab:IsVisible() ) then
          AutoProfitX_SellTab:Hide();
     end
end

-- AutoProfitX:idUtils( )
-- =========================================================
function AutoProfitX:idUtils( action, v1, v2, items )
     local itemID, v1u, wasOpen;
     v1u = string.upper( v1 );
     if ( AutoProfitX.db.global.frameOpen and v2 ) then
          AutoProfitXFrame:Hide();
          AutoProfitXFrame = nil;
          wasOpen = true;
     end
     if ( action == "idCheck" ) then
          itemID = AutoProfitX:GetID( link );
          if ( AutoProfitX.db.global[v1][itemID] ) then return true; end
          if ( AutoProfitX.db.global[v1][itemID] ) then return true; end
          return false;
     elseif ( action == "idRemove" ) then
          for link in string.gmatch( items, L["APX_FORMATED_LM"] ) do
	       itemID = AutoProfitX:GetID( link );
               if ( itemID ) then
                    AutoProfitX:Print( "print", L["APX_FORMATED_REMOVE"]:format( link, v1 ) );
                    AutoProfitX.db.global[v1][itemID] = nil;
               else
                    AutoProfitX:Print( "print", L["APX_OPTIONS_BADLINK"] );
               end
          end
     elseif ( action == "idAdd" ) then
          for link in string.gmatch( items, L["APX_FORMATED_LM"] ) do
               itemID = AutoProfitX:GetID( link );
               if ( itemID ) then
                    AutoProfitX:Print( "print", L["APX_FORMATED_ADD"]:format( link, v1 ) );
                    AutoProfitX.db.global[v1][itemID] = {
                         t = select( 10, GetItemInfo( link ) ),
                         l = link,
                    }
               else
                    AutoProfitX:Print( "print", L["APX_OPTIONS_BADLINK"] );
               end
          end
     end
     if ( wasOpen and v2 ) then AutoProfitX:MakeFrame( v1 ); end
     if ( AutoProfitX_SellTab:IsVisible() ) then
          AutoProfitX:OnShowButton();
     end
end

-- AutoProfitX:ListExceptions( value )
-- =========================================================
function AutoProfitX:ListExceptions( value )
     local lType = string.upper( value );

     local eList = AutoProfitX.db.global[value];

     local dispHeader = true;
     for i, t in pairs( eList ) do
          if ( dispHeader ) then
               self:Print( "print", L[ ( "APX_LIST_%s" ):format( lType ) ] );
               dispHeader = false
          end
          local link = select( 2, GetItemInfo( i ) );
          if ( link ) then
               self:Print( "print", link );
          end
     end
     if ( dispHeader ) then
          self:Print( "print", L[ ( "APX_LIST%s_EMPTY" ):format( lType ) ] );
     end
end

-- AutoProfitX:Phase( text )
-- =========================================================
function AutoProfitX:Phase( text )
     text = string.gsub( text, "{LEATHER}", L["APX_ARMOR_LEATHER"] );
     text = string.gsub( text, "{MAIL}", L["APX_ARMOR_MAIL"] );
     text = string.gsub( text, "{PLATE}", L["APX_ARMOR_PALTE"] );
     text = string.gsub( text, "{CLOTH}", L["APX_ARMOR_CLOTH"] );
     text = string.gsub( text, "{NONE}", L["APX_ARMOR_NONE"] );
     return text;
end

-- AutoProfitX:GetID( link )
-- =========================================================
function AutoProfitX:GetID( Link )
     return string.match( Link, "item:(%d+)" );
end

-- AutoProfitX:FillArray()
-- =========================================================
function AutoProfitX:FillArray( link, id, q, sell )
     if ( AutoProfitX.db.char.debug.state ) then
          AutoProfitX.db.char.debug.sList[id] = {
               c = select( 8, GetItemInfo( link ) ),
               l = link,
               p = AutoProfitX:GetMoneyString( select( 11, GetItemInfo( link ) ), true ),
               s = "",
               t = select( 10, GetItemInfo( link ) ),
          }
          if ( sell == true ) then
               AutoProfitX.db.char.debug.sList[id].s = "Sellable"
          else
               AutoProfitX.db.char.debug.sList[id].s = "Non-Sellable"
          end
     end
end

-- AutoProfitX:SellJunk()
-- =========================================================
function AutoProfitX:SellJunk()
     local link, money
     if ( MerchantFrame.selectedTab == 1 ) then
          for bag = 0, 5 do
               for slot = 1, C_Container.GetContainerNumSlots( bag ) do
                    link = C_Container.GetContainerItemLink( bag, slot );
                    if ( link and AutoProfitX:IsJunk( bag, slot, link ) ) then
                         iValue = select( 11, GetItemInfo( link ) )
                         if ( iValue > 0 ) then
                              -- if slot not empty and item is junk then sell item
                              -- =============================
                              C_Container.UseContainerItem( bag, slot );
                              -- UseContainerItem( bag, slot );
                              -- =============================
                              -- if sale reporting is turned on ( not silent ), display sale message
                              if ( AutoProfitX.db.char.silent ) then
                                   money = select( 11, GetItemInfo( link ) ) * C_Container.GetContainerItemInfo( bag, slot ).stackCount;
                                   if ( AutoProfitX.db.char.fancysales ) then
                                        if ( AutoProfitX.db.char.stackCount ) then
                                             self:Print( "print",  format( L["APX_FORMATED_COINSTACK"],  link, GetItemCount( link ), self:GetMoneyString( money, AutoProfitX.db.char.profits ) ) );
                                        else
                                             self:Print( "print", format( L["APX_FORMATED_COIN"], link, self:GetMoneyString( money, AutoProfitX.db.char.profits ) ) );
                                        end
                                   else
                                        if ( AutoProfitX.db.char.stackCount ) then
                                             self:Print( "print", format( L["APX_FORMATED_TEXTSTACK"], link, GetItemCount( link ) ) );
                                        else
                                             self:Print( "print", format( L["APX_FORMATED_TEXT"], link ) );
                                        end
                                   end
                              end
                         end
                    end
               end
          end
     end
end

-- AutoProfit:IsJunk()
-- =========================================================
function AutoProfitX:IsJunk( bag, slot, link )
     local id = AutoProfitX:GetID( link );
     local quality = select( 3, GetItemInfo( link ) );
     local level = UnitLevel( "player" );
     if ( AutoProfitX.db.global.Exemptions[id] ) then
          AutoProfitX:FillArray( link, id, quality, false );
          return false;
     end
     if ( quality == 0 ) then
          AutoProfitX:FillArray( link, id, quality, true );
          return true;
     else
          if ( AutoProfitX.db.global.Exceptions[id] ) then
               AutoProfitX:FillArray( link, id, quality, true );
               return true;
          end
          if ( AutoProfitX.db.char.checkSoulbound ) then
               if ( AutoProfitX:IsUsable( bag, slot, link ) == false ) then
                    AutoProfitX:FillArray( link, id, quality, true );
                    return true;
               end
          else
               if ( AutoProfitX.db.char.sellarmorsub and level >= 40 ) then
                    if ( AutoProfitX:CheckSubArmor( bag, slot, link ) == false ) then
                         AutoProfitX:Print( "bug", link );
                         AutoProfitX:FillArray( link, id, quality, true );
                         return true;
                    end
               else
                    AutoProfitX:FillArray( link, id, quality, false );
                    return false;
               end
          end
     end
end

-- AutoProfitX:IsUsable()
-- =========================================================
function AutoProfitX:IsUsable( bag, slot, link )
     local classL, classE = UnitClass( "player" ) -- check if item has class requirement
     local level = UnitLevel( "player" );
     local iType, iSubType, iEquipLoc = string.upper( select( 6, GetItemInfo( link ) ) ), select( 7, GetItemInfo( link ) ), select( 9, GetItemInfo( link ) );
     local satbl
     if ( iType == "ARMOR" and AutoProfitX.db.char.sellarmorsub ) then
          satbl = AutoProfitX_ArmorSub[classE][iType]
     else
          satbl = {};
          satbl[iSubType] = false;
     end
     scanTip:ClearLines();
     scanTip:SetBagItem( bag, slot );
     if ( ( not AutoProfitX:scanTip( ITEM_SOULBOUND ) ) and ( not satbl[iSubType] ) ) then
          -- if it's not soulbound then you can always use it
          -- but is sell sub armor then ignore
          return true;
     end
     if ( AutoProfitX:scanTip( L["APX_FORMATED_IES"] ) ) then -- equipMatch ) ) then
          -- SLAM ON DA BREAKS -- this is part of someone gear set
          return true;
     end
     local _, _, classstr = AutoProfitX:scanTip( classMatch );
     if ( classstr ) then
          local found = false
          classstr = { strsplit( ", ", classstr ) };
          for _, c in ipairs( classstr ) do
               if ( string.match( c, classL ) ) then
                    found = true;
               end
          end
          -- if ( not found ) then return false; end -- class not in the list so you can't use it
          return found;
     end
     local tbl = prof[iType]
     AutoProfitX.db.char.debug.temp = tbl;
     if ( tbl and ( tbl[iSubType] or ( tbl.noOffhand and iEquipLoc == "INVTYPE_WEAPONOFFHAND" ) ) ) then
          return false;
     end

     -- If this is enabled then all armor sub classes will considered as Unusable
     if ( AutoProfitX.db.char.sellarmorsub and iType == "ARMOR" and level >= 40 ) then if ( satbl[iSubType] ) then return false; end end
     -- Seems Un-Usable
     return false;
end

-- AutoProfitX:CheckSubArmor( bag, slot, link )
-- =========================================================
function AutoProfitX:CheckSubArmor( bag, slot, link )
     local classE = select( 2, UnitClass( "player" ) );
     local level = UnitLevel( "player" );
     local iType, iSubType, iEquipLoc = string.upper( select( 6, GetItemInfo( link ) ) ), select( 7, GetItemInfo( link ) ), select( 9, GetItemInfo( link ) );
     local satbl, iCheck
     if ( iType == "ARMOR" and AutoProfitX.db.char.sellarmorsub ) then
          satbl = AutoProfitX_ArmorSub[classE][iType]
     else
          satbl = {};
          satbl[iSubType] = false;
     end
     scanTip:ClearLines();
     scanTip:SetBagItem( bag, slot );
     if ( AutoProfitX.db.char.sellarmorsub and iType == "ARMOR" and level >= 40 ) then
          if ( satbl[iSubType] ) then
               iCheck = AutoProfitX:scanTip( ITEM_SOULBOUND );
               if ( iEquipLoc ~= "INVTYPE_CLOAK" and not iCheck ) then return false; end
          else
               return true;
          end
     end
end

-- AutoProfitX:GetProfit( tooltip )
-- =========================================================
function AutoProfitX:GetProfit( tooltip )
     totalProfit = 0;
     local isOpen, curTab
     if ( tooltip == 1 ) then
          isOpen = true;
          curTab = 1;
     end
     -- if ( isOpen and curTab == 1 ) then
          local bagSlots, link
          for bag = 0, 4 do
               bagSlots = C_Container.GetContainerNumSlots( bag );
               if ( bagSlots > 0 ) then
                    for slot = 1, bagSlots do
                         link = C_Container.GetContainerItemLink( bag, slot );
                         if ( link and AutoProfitX:IsJunk( bag, slot, link ) ) then
                              -- unlike posted by user AlexL1118 on curseforge the money var does nothing for us in this function
                              local v = select( 11, GetItemInfo( link ) );
                              local c = C_Container.GetContainerItemInfo( bag, slot ).stackCount;
                              local price = v * c;
                              totalProfit = totalProfit + price;
                         end
                    end
               end
          end
     -- end
     return totalProfit;
end

-- AutoProfitX:OnEnterButton( self )
-- =========================================================
function AutoProfitX:OnEnterButton( self )
     GameTooltip:SetOwner( self, "ANCHOR_RIGHT" );
     GameTooltip:SetText( L["APX_BUTTON_SELLJUNK"] );
     local profit = AutoProfitX:GetProfit( 0 );
     if ( AutoProfitX.db.char.buttonSpin == 2 ) then AutoProfitX:UpdateButton(); end
     if ( profit > 0 ) then
          if ( AutoProfitX.db.char.buttonSpin == 1 ) then AutoProfitX:UpdateButton(); end
          GameTooltip:AddLine( AutoProfitX:GetMoneyString( profit, AutoProfitX.db.char.profits ) )
     else
          GameTooltip:AddLine( L["APX_BUTTON_NOJUNK"], 1.0, 1.0, 1.0, 1 );
     end
     GameTooltip:AddLine( L["APX_BUTTON_OPTIONS"] );
     GameTooltip:Show();
end

-- AutoProfitX:OnLeaveButton()
-- =========================================================
function AutoProfitX:OnLeaveButton()
     GameTooltip:Hide();
     if ( AutoProfitX.db.char.buttonSpin ~= 3 ) then AutoProfitX:UpdateButton( "stop" ); end
end

-- AutoProfitX:OnClickButton()
-- =========================================================
function AutoProfitX:OnClickButton( _, button )
     if ( button == "LeftButton" ) then
          local profit = AutoProfitX:GetProfit( 0 );
          if ( profit > 0 ) then
               GameTooltip:Hide();
               AutoProfitX:SellJunk();
               if ( AutoProfitX.db.char.showTotal ) then
                    self:Print( "print", format( L["APX_BUTTON_PRINT_PROFIT"], AutoProfitX:GetMoneyString( profit, AutoProfitX.db.char.profits ) ) );
               end
               AutoProfitX:ButtonState();
          end
     else
          InterfaceOptionsFrame_OpenToCategory( AutoProfitX.ConfigFrames.AutoProfitX );
     end
end

-- AutoProfitX:OnShowButton()
-- =========================================================
function AutoProfitX:OnShowButton()
     if ( AutoProfitX.db.char.autoSell ) then
          AutoProfitX_SellTab:Hide();
     else
          AutoProfitX_SellTab:Show();
     end
     if ( AutoProfitX.db.char.buttonSpin == 3 and AutoProfitX:GetProfit( 0 ) > 0 ) then
          AutoProfitX:UpdateButton();
     end
end

-- AutoProfitX:ButtonState()
-- =========================================================
function AutoProfitX:ButtonState()
     -- Needed for OnEnterButton
     if ( AutoProfitX.db.char.buttonSpin == 1 ) then
          AutoProfitX:UpdateButton();
     end
     -- Needed for OnLeaveButton
     if ( AutoProfitX.db.char.buttonSpin ~= 3 ) then
          AutoProfitX:UpdateButton( "stop" );
     end
     -- Needed for OnClick
     if ( AutoProfitX.db.char.buttonSpin ~= 2 ) then
          AutoProfitX:UpdateButton( "stop" );
     end
     -- Needed for OnShowButton
     if ( AutoProfitX.db.char.buttonSpin == 3 and AutoProfitX:GetProfit( 0 ) > 0 ) then
          AutoProfitX:UpdateButton();
     end
end

-- AutoProfitX:AddTooltipMoney( S )
-- =========================================================
function AutoProfitX:AddTooltipMoney( S )
     if ( not totalProfit or totalProfit == nil ) then totalProfit = 0; end
     SetTooltipMoney( S, totalProfit, "STATIC", "Junk Value : ", totalProfit );
     return true;
end

function AutoProfitX:theMagic( S )
     for Key, Value in pairs( S ) do
          print( ( "[%s] = "):format( Key ) );
          if ( type( Value ) == "table" ) then
               print( ( "[%s] = "):format( Key ) );
               for key, value in pairs( Value ) do
                    -- print( ( "[%s] = "):format( key ) );
               end
               print( "end" );
          else
               print( ( "%s," ):format( Value ) );
          end
          print( "end" );
     end
end

-- AutoProfitX:UpdateButton( action, spinRate )
-- =========================================================
function AutoProfitX:UpdateButton( Action, spinRate )
     if ( Action == "stop" ) then
          AutoProfitX_SellTab_Button_TreasureModel.rotRate = 0;
     else
          spinRate = tonumber( spinRate );
          if ( not spinRate ) then spinRate = AutoProfitX.db.char.spinRate end
          AutoProfitX_SellTab_Button_TreasureModel.rotRate = spinRate;
     end
end

-- AutoProfitX:Print( Action, Msg )
-- =========================================================
function AutoProfitX:Print( Action, Msg )
     if ( Action == "bug" and AutoProfitX.db.char.debug.state ) then
          DEFAULT_CHAT_FRAME:AddMessage( format( L["APX_PRINT_DEBUGLINE"], Msg) );
     elseif ( Action == "print" ) then
          DEFAULT_CHAT_FRAME:AddMessage( format( L["APX_PRINT_PRINTLINE"], Msg ) );
     elseif ( Action == "error" ) then
          DEFAULT_CHAT_FRAME:AddMessage( format( L["APX_PRINT_ERRORLINE"], Msg ) );
     end
end

-- AutoProfitX:ButtonClick( Action )
-- =========================================================
function AutoProfitX:ButtonClick( Action )
     local CheckS, CheckA = IsShiftKeyDown(), IsAltKeyDown();
     if ( Action == "config" ) then
          InterfaceOptionsFrame_OpenToCategory( AutoProfitX.ConfigFrames.AutoProfitX );
     elseif ( Action == "list" ) then
          if ( AutoProfitX.db.char.debug.state and CheckA and CheckS ) then
               ReloadUI(); -- end
          else
               if ( not AutoProfitXFrame ) then
                    AutoProfitX:MakeFrame();
               else
                    AutoProfitXFrame:Hide();
                    AutoProfitXFrame = nil;
               end
          end
     end
end

-- AutoProfitX:GetMoneyString( Money, pType )
-- =========================================================
function AutoProfitX:GetMoneyString( Money, pType )
     local gold = floor( Money / 10000 );
     local silver = floor( ( Money - gold * 10000 ) / 100 );
     local copper = mod( Money, 100 );
     local g, s, c, totalStr, mClr;
     mClr = AutoProfitX.db.char.cText;
     if ( pType == 2 ) then
          if ( gold > 0 ) then
               g = L[( "APX_GOLD_%s_COIN" ):format( mClr )]:format( gold, 0, 0 );
               s = L[( "APX_SILVER_%s_COIN" ):format( mClr )]:format( silver, 0, 0 );
               c = L[( "APX_COPPER_%s_COIN" ):format( mClr )]:format( copper, 0, 0 );
               totalStr = ( "%s %s %s" ):format( g, s, c );
          elseif ( silver > 0 ) then
               s = L[( "APX_SILVER_%s_COIN" ):format( mClr )]:format( silver, 0, 0 );
               c = L[( "APX_COPPER_%s_COIN" ):format( mClr )]:format( copper, 0, 0 );
               totalStr = ( "%s %s" ):format( s, c );
          else
               c = L[( "APX_COPPER_%s_COIN" ):format( mClr )]:format( copper, 0, 0 );
               totalStr = ( "%s" ):format( c );
          end
     elseif ( pType == 1 ) then
          if ( gold > 0 ) then
               g = L[( "APX_GOLD_%s_TEXT" ):format( mClr )]:format( gold );
               s = L[( "APX_SILVER_%s_TEXT" ):format( mClr )]:format( silver );
               c = L[( "APX_COPPER_%s_TEXT" ):format( mClr )]:format( copper );
               totalStr = ( "%s %s %s" ):format( g, s, c );
          elseif ( silver > 0 ) then
               s = L[( "APX_SILVER_%s_TEXT" ):format( mClr )]:format( silver );
               c = L[( "APX_COPPER_%s_TEXT" ):format( mClr )]:format( copper );
               totalStr = ( "%s %s" ):format( s, c );
          else
               c = L[( "APX_COPPER_%s_TEXT" ):format( mClr )]:format( copper );
               totalStr = ( "%s" ):format( c );
          end
     end
     return totalStr;
end

-- AutoProfitX:OpenAllBags( Close )
-- =========================================================
function AutoProfitX:OpenAllBags( Close )
     for bag = 0, 5 do
          if ( not Close ) then
               if ( not IsBagOpen( bag ) ) then
                    AutoProfitX.db.global.bag[bag] = true;
                    OpenBag( bag );
               end
          elseif AutoProfitX.db.global.bag[bag] then
                    AutoProfitX.db.global.bag[bag] = false;
                    CloseBag( bag );
          end
     end
end

-- AutoProfitX:otherBag()
-- =========================================================
function AutoProfitX:otherBag()
     local otherBag = false;
     if ( IsAddOnLoaded( "BaudBag" ) ) then otherBag = true; end
     if ( IsAddOnLoaded( "ArkInventory" ) ) then otherBag = true; end
     if ( IsAddOnLoaded( "Bagnon" ) ) then otherBag = true; end
     if ( IsAddOnLoaded( "OneBag3" ) ) then otherBag = true; end
     if ( IsAddOnLoaded( "Baggins" ) ) then otherBag = true; end
     return otherBag;
end

-- AutoProfitX:scanTip( sText )
-- =========================================================
function AutoProfitX:scanTip( sText )
     for id = 1, scanTip:NumLines() do
          local text = _G[( "AutoProfitXScanTipTextLeft%s" ):format( id )];
          tStr = text:GetText();
          if ( tStr and string.find( tStr, sText ) ) then
               return string.find( tStr, sText );
          end
     end
end


-- AutoProfitX:ConcentSellSoulbound()
-- =========================================================

function AutoProfitX:ConcentSellSoulbound( value )
     StaticPopupDialogs["APX_CONCENT_SOULBOUND_NOTE"] = {
          text = L["APX_CONCENT_SOULBOUND_WARNING"],
          button1 = ACCEPT,
          button2 = CANCEL,

          OnAccept = function( self )
               AutoProfitX.db.char.checkSoulbound = value;
               InterfaceOptionsFrame_OpenToCategory( AutoProfitX.ConfigFrames.AutoProfitX );
               AutoProfitX:ButtonState();
          end,
          OnCancel = function( self )
               AutoProfitX.db.char.checkSoulbound = not value;
               InterfaceOptionsFrame_OpenToCategory( AutoProfitX.ConfigFrames.AutoProfitX );
               AutoProfitX:ButtonState();
          end,
          showAlertGear = 1,
          timeout = 0,
          whileDead = 1,
          hideOnEscape = 1

     };
     PlaySound( "ReadyCheck" );
     StaticPopup_Show( "APX_CONCENT_SOULBOUND_NOTE", value );
end

-- AutoProfitX:ReSetDB()
-- =========================================================

function AutoProfitX:ReSetDB( label, curver, ver )
     StaticPopupDialogs["APX_RESET_DB"] = {
          text = L["APX_RESET_DB"]:format( label, curver, ver ),
          button1 = ACCEPT,
          button2 = nil,

          OnAccept = function( self )
               AutoProfitX.db.global.Version = curver;
               if ( AutoProfitX.db.char.debug.state ) then ReloadUI(); end
          end,
          showAlert = 1,
          timeout = 0,
          whileDead = 1,
          hideOnEscape = 1,
     };
     StaticPopup_Show( "APX_RESET_DB", label, curver, ver );
end