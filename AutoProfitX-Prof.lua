--list of items that classes cannot use
local BI = LibStub("LibBabble-Inventory-3.0"):GetLookupTable()

-- local AutoProfitX_Proficiencies
-- =========================================================
AutoProfitX_Proficiencies = {
	["DRUID"] = {
		[ARMOR] = {
			[BI["Mail"]] = true,
			[BI["Shields"]] = true,
			[BI["Plate"]] = true,
			[BI["Librams"]] = true,
			[BI["Totems"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Bows"]] = true,
			[BI["Crossbows"]] = true,
			[BI["Guns"]] = true,
			[BI["One-Handed Axes"]] = true,
			[BI["One-Handed Swords"]] = true,
			[BI["Thrown"]] = true,
			[BI["Two-Handed Axes"]] = true,
			[BI["Two-Handed Swords"]] = true,
			[BI["Wands"]] = true,
			noOffhand = true,
		},
	},
	["HUNTER"] = {
		[ARMOR] = {
			[BI["Shields"]] = true,
			[BI["Plate"]] = true,
			[BI["Idols"]] = true,
			[BI["Librams"]] = true,
			[BI["Totems"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["One-Handed Maces"]] = true,
			[BI["Two-Handed Maces"]] = true,
			[BI["Wands"]] = true,
		},
	},
	["MAGE"] = {
		[ARMOR] = {
			[BI["Leather"]] = true,
			[BI["Mail"]] = true,
			[BI["Shields"]] = true,
			[BI["Plate"]] = true,
			[BI["Idols"]] = true,
			[BI["Librams"]] = true,
			[BI["Totems"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Bows"]] = true,
			[BI["Crossbows"]] = true,
			[BI["Guns"]] = true,
			[BI["Fist Weapons"]] = true,
			[BI["One-Handed Axes"]] = true,
			[BI["One-Handed Maces"]] = true,
			[BI["Polearms"]] = true,
			[BI["Thrown"]] = true,
			[BI["Two-Handed Axes"]] = true,
			[BI["Two-Handed Maces"]] = true,
			[BI["Two-Handed Swords"]] = true,
			noOffhand = true,
		}
	},
	["PALADIN"] = {
		[ARMOR] = {
			[BI["Librams"]] = true,
			[BI["Idols"]] = true,
			[BI["Totems"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Bows"]] = true,
			[BI["Crossbows"]] = true,
			[BI["Guns"]] = true,
			[BI["Fist Weapons"]] = true,
			[BI["Thrown"]] = true,
			[BI["Wands"]] = true,
			noOffhand = true,
		},
	},
	["PRIEST"] = {
		[ARMOR] = {
			[BI["Leather"]] = true,
			[BI["Mail"]] = true,
			[BI["Shields"]] = true,
			[BI["Plate"]] = true,
			[BI["Idols"]] = true,
			[BI["Librams"]] = true,
			[BI["Totems"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Bows"]] = true,
			[BI["Crossbows"]] = true,
			[BI["Guns"]] = true,
			[BI["Fist Weapons"]] = true,
			[BI["One-Handed Axes"]] = true,
			[BI["One-Handed Swords"]] = true,
			[BI["Polearms"]] = true,
			[BI["Thrown"]] = true,
			[BI["Two-Handed Axes"]] = true,
			[BI["Two-Handed Maces"]] = true,
			[BI["Two-Handed Swords"]] = true,
			noOffhand = true
		},
	},
	["ROGUE"] = {
		[ARMOR] = {
			[BI["Mail"]] = true,
			[BI["Shields"]] = true,
			[BI["Plate"]] = true,
			[BI["Idols"]] = true,
			[BI["Librams"]] = true,
			[BI["Totems"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Polearms"]] = true,
			[BI["Staves"]] = true,
			[BI["Two-Handed Axes"]] = true,
			[BI["Two-Handed Maces"]] = true,
			[BI["Two-Handed Swords"]] = true,
			[BI["Wands"]] = true,
			[BI["Thrown"]] = true,
		}
	},
	["SHAMAN"] = {
		[ARMOR] = {
			[BI["Plate"]] = true,
			[BI["Idols"]] = true,
			[BI["Librams"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Bows"]] = true,
			[BI["Crossbows"]] = true,
			[BI["Guns"]] = true,
			[BI["One-Handed Swords"]] = true,
			[BI["Polearms"]] = true,
			[BI["Thrown"]] = true,
			[BI["Two-Handed Swords"]] = true,
			[BI["Wands"]] = true,
		},
	},
	["WARLOCK"] = {
		[ARMOR] = {
			[BI["Leather"]] = true,
			[BI["Mail"]] = true,
			[BI["Shields"]] = true,
			[BI["Plate"]] = true,
			[BI["Idols"]] = true,
			[BI["Librams"]] = true,
			[BI["Totems"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Bows"]] = true,
			[BI["Crossbows"]] = true,
			[BI["Guns"]] = true,
			[BI["Fist Weapons"]] = true,
			[BI["One-Handed Axes"]] = true,
			[BI["One-Handed Maces"]] = true,
			[BI["Polearms"]] = true,
			[BI["Thrown"]] = true,
			[BI["Two-Handed Axes"]] = true,
			[BI["Two-Handed Maces"]] = true,
			[BI["Two-Handed Swords"]] = true,
			noOffhand = true
		},
	},
	["WARRIOR"] = {
		[ARMOR] = {
			[BI["Idols"]] = true,
			[BI["Librams"]] = true,
			[BI["Totems"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Wands"]] = true,
			[BI["Thrown"]] = true,
			[BI["Guns"]] = true,
			[BI["Bows"]] = true,
			[BI["Crossbows"]] = true,
		},
	},
	["DEATHKNIGHT"] = {
		[ARMOR] = {
			[BI["Idols"]] = true,
			[BI["Librams"]] = true,
			[BI["Totems"]] = true,
                        [BI["Shields"]] = true,
		},
		[ENCHSLOT_WEAPON] = {
			[BI["Wands"]] = true,
			[BI["Bows"]] = true,
			[BI["Crossbows"]] = true,
			[BI["Guns"]] = true,
			[BI["Thrown"]] = true,
			[BI["Staves"]] = true,
                        [BI["Daggers"]] = true,
			[BI["Fist Weapons"]] = true,
		},
	},
}

-- AutoProfitX_ArmorSub
-- =========================================================
AutoProfitX_ArmorSub = {
     DEATHKNIGHT = {
          ARMOR = {
               Mail = true,
               Leather = true,
               Cloth = true,
          },
     },
     PALADIN = {
          ARMOR = {
               Mail = true,
               Leather = true,
               Cloth = true,
          },
     },
     WARRIOR = {
          ARMOR = {
               Mail = true,
               Leather = true,
               Cloth = true,
          },
     },
     HUNTER = { 
          ARMOR = {
               Leather = true,
               Cloth = true,
          },
     },
     SHAMAN = {
          ARMOR = {
               Cloth = true,
          },
     },
     DRUID = {
          ARMOR = {
               Cloth = true,
          },
     },
     ROGUE = {
          ARMOR = {
               Cloth = true,
          },
     },
     MAGE = {
          ARMOR = {
               Cloth = false,
          },
     },
     PRIEST = {
          ARMOR = {
               Cloth = false,
          },
     },
     WARLOCK = {
          ARMOR = {
               Cloth = false,
          },
     },
}

-- local AutoProfitX_SubArmor
-- =========================================================
AutoProfitX_SubArmor = {
     DEATHKNIGTH = [=[

          |cffc41f3b%s|r :
               {MAIL}
               {LEATHER}
               {CLOTH}
     ]=],
     PALADIN = [=[

          |cfff58cba%s|r :
               {MAIL}
               {LEATHER}
               {CLOTH}
     ]=],
     WARRIOR = [=[

          |cffc79c6e%s|r :
               {MAIL}
               {LEATHER}
               {CLOTH}
     ]=],
     HUNTER = [=[

          |cffabd473%s|r :
               {LEATHER}
               {CLOTH}
     ]=],
     SHAMAN = [=[

          |cff0070de%s|r :
               {CLOTH}
     ]=],
     DRUID = [=[

          |cffff7d0a%s|r :
               {CLOTH}
     ]=],
     ROGUE = [=[

          |cfffff569%s|r :
               {CLOTH}
     ]=],
     MAGE = [=[

          |cff69CCF0%s|r :
               {NONE}
     ]=],
     PRIEST = [=[

          |cffffffff%s|r :
               {NONE}
     ]=],
     WARLOCK = [=[

          |cff9482C9%s|r :
               {NONE}
     ]=],
}