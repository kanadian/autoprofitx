------------------------------------------------------------------------------------------
-- AutoProfitX-Config - r1
--
-- Dialog for Exceptions / Exemptions and Debugging
--
-- Send suggestions, comments, and bugs to bug@kanadian.net.
------------------------------------------------------------------------------------------

local AddOnName = "AutoProfitX";
local L = LibStub( "AceLocale-3.0" ):GetLocale( AddOnName, false );
local AceGUI = LibStub( "AceGUI-3.0" );

local treeData = {
     {
          value = L["APX_FRAME_EXCEPTIONS"],
          text = L["APX_FRAME_EXCEPTIONS"],
          disabled = false,
     },
     {
          value = L["APX_FRAME_EXEMPTIONS"],
          text = L["APX_FRAME_EXEMPTIONS"],
          disabled = false,
     },
     {
          value = L["APX_DEBUG_OPTDEBUG"],
          text = L["APX_DEBUG_OPTDEBUG"],
          disabled = true,
     },
}

local OnGroupSelected = {
     Core = function( branch, v1 )
          local v1u = string.upper( v1 )
          local grp1 = AceGUI:Create( "SimpleGroup" );
          grp1:SetFullWidth( true );
          grp1:SetLayout( "Flow" );
          local header = AceGUI:Create( "Heading" );
          header:SetText( L[ ( "APX_FRAME_%s" ):format( v1u ) ] );
          header:SetRelativeWidth( 1 );
          local descT = AceGUI:Create( "Label" );
          descT:SetText( L[ ( "APX_FRAME_%s_DESC" ):format( v1u ) ] );
          descT:SetRelativeWidth( 1 );
          local add = AceGUI:Create( "EditBox" );
          add:DisableButton( true );
          add:SetLabel( L["APX_FRAME_ADD" ]:format( v1 ) );
          add:SetCallback( "OnEnterPressed", function( widget, event, value )
                    AutoProfitX:idUtils( "idAdd", v1, false, value );
                    add:SetText( "" );
                    AutoProfitXFrame:Hide();
                    AutoProfitXFrame = nil;
                    AutoProfitX:MakeFrame( v1 );
                    AutoProfitX:ButtonState();
               end
          )
          local remove = AceGUI:Create( "EditBox" );
          remove:DisableButton(true);
          remove:SetLabel( L[ "APX_FRAME_REMOVE" ]:format( v1 ) );
          remove:SetCallback( "OnEnterPressed", function( widget, event, value )
                    AutoProfitX:idUtils( "idRemove", v1, false, value );
                    remove:SetText( "" );
                    AutoProfitXFrame:Hide();
                    AutoProfitXFrame = nil;
                    AutoProfitX:MakeFrame( v1 );
                    AutoProfitX:ButtonState();
               end
          )
          local grp2 = AceGUI:Create( "SimpleGroup" );
          grp2:SetFullWidth( true );
          grp2:SetFullHeight( true );
          grp2:SetLayout( "Fill" );
          local scroll = AceGUI:Create( "ScrollFrame" );
          scroll:SetRelativeWidth( 1 );
          scroll:SetLayout( "Flow" );
          for _, v in pairs( AutoProfitX.db.global[ v1 ] ) do
               local item = AceGUI:Create( "InteractiveLabel" );
               item:SetRelativeWidth( 1 );
               item:SetImage( v.t );
               item:SetImageSize( 32, 32 );
               item:SetHighlight( 1, 0.5, 1, 0.4 );
               item:SetFont( "Fonts\\FRIZQT__.TTF", 14, "OUTLINE, MONOCHROME" );
               item:SetText( v.l );
               scroll:AddChild( item );
          end
          grp1:AddChildren( header, descT, add, remove );
          grp2:AddChild( scroll );
          branch:AddChildren( grp1, grp2 );
     end,
     Debug = function( branch )
          branch:ReleaseChildren();
          local grp1 = AceGUI:Create( "SimpleGroup" );
          grp1:SetFullWidth( true );
          grp1:SetLayout( "Flow" );
          local about = AceGUI:Create( "Heading" );
          about:SetText( L["APX_DEBUG_OPTDEBUG"] );
          about:SetRelativeWidth( 1 );
          local grp2 = AceGUI:Create( "SimpleGroup" );
          grp2:SetFullWidth( true );
          grp2:SetFullHeight( true );
          grp2:SetLayout( "Fill" );
          local scrollds = AceGUI:Create( "ScrollFrame" );
          scrollds:SetRelativeWidth( 1 );
          scrollds:SetFullHeight( true );
          scrollds:SetLayout( "Flow" );
          for _, v in pairs(  AutoProfitX.db.char.debug.sList ) do
               local dslitem = AceGUI:Create( "InteractiveLabel" );
               dslitem:SetRelativeWidth( 1 );
               dslitem:SetImage( v.t );
               dslitem:SetImageSize( 32, 32 );
               dslitem:SetHighlight( 1, 0.5, 1, 0.4 );
               dslitem:SetFont( "Fonts\\FRIZQT__.TTF", 14, "OUTLINE, MONOCHROME" );
               dslitem:SetText( ( "%s x %s\n%s %s" ):format( v.l, v.c, v.s, v.p ) );
               scrollds:AddChild( dslitem );
          end
          grp1:AddChildren( about );
          grp2:AddChild( scrollds );
          branch:AddChildren( grp1, grp2 );
     end,
}

function AutoProfitX:MakeFrame( oThis )
     local v
     if ( not oThis ) then oThis = L["APX_FRAME_EXCEPTIONS"]; end
     if ( _G["apxArray"].Author[ ( "%s@%s" ):format( GetUnitName( "player" ), GetRealmName() ) ] ) then treeData[ 3 ].disabled = false; end
     if ( apx ) then AceGUI:Release( apx ); end
     local apx = AceGUI:Create( "Window" );
     apx:SetTitle( L["APX_PRINT_PRINTLINE"]:format( GetAddOnMetadata( AddOnName, "Version" ) ) );
     apx:EnableResize( false );
     apx:SetWidth( 640 ); -- 435
     apx:SetHeight( 480 ); -- 310
     apx:SetLayout( "Flow" );
     apx:SetCallback( "OnClose", function( apx )
               AceGUI:Release( apx );
               AutoProfitXFrame = nil;
               AutoProfitX.db.global.frameOpen = false;
               collectgarbage();
          end
     )
     AutoProfitXFrame = apx.frame;
     AutoProfitX.db.global.frameOpen = true;
     local tree = AceGUI:Create( "TreeGroup" );
     tree:SetRelativeWidth( 1.0 );
     tree:SetLayout( "Flow" );
     tree:EnableButtonTooltips( false );
     tree:SetTree( treeData );
     tree:SetCallback( "OnGroupSelected", function( widget, event, value )
               tree:ReleaseChildren();
               local v
               if ( value == "Debug" ) then
                    v = "Debug";
               else
                    v = "Core";
               end
               AutoProfitX.db.global.openFrame = true;
               OnGroupSelected[ v ]( widget, value );
          end
     );
     tree:SetFullHeight( true );
     if ( oThis ) then tree:SelectByPath( oThis ); end
     apx:AddChild( tree );
     apx:Show();
end