﻿local L = LibStub( "AceLocale-3.0" ):NewLocale( "AutoProfitX", "zhCN" );
if ( not L ) then return; end 

L["APX_ABOUT_ABOUT"] = "About" -- Requires localization
L["APX_ABOUT_ABOUT_DESC"] = "Automatically sell junk items when you click the button on the vendors window." -- Requires localization
L["APX_ABOUT_AUTHOR"] = "|cffffd700Author :|r |cffff8c00 %s|r" -- Requires localization
L["APX_ABOUT_CATEGORY"] = "|cffffd700Category :|r |cffff8c00 %s|r" -- Requires localization
L["APX_ABOUT_EMAIL"] = "|cffffd700Email :|r |cffff8c00 %s|r" -- Requires localization
L["APX_ABOUT_LDB_DESC"] = [=[ 
|cffc0c0c0The Minimap icon is a LDBIcon stream, You can disable the Minimap icon but enable an Infobar Button.

     Example : This will Add an AutoProfitX button to APX Panel|r
]=] -- Requires localization
L["APX_ABOUT_LOAD"] = "Addon Load Message" -- Requires localization
L["APX_ABOUT_LOAD_DESC"] = "This will display a message in the chat frame when then addon is fully loaded." -- Requires localization
L["APX_ABOUT_MEMORY"] = "|cffffd700Memory Usage :|r |cffffffff%skb|r" -- Requires localization
L["APX_ABOUT_MEMORY_DESC"] = "This will display memory usgae in the chat frame." -- Requires localization
L["APX_ABOUT_MINIMAP"] = "Hide Minimap Button" -- Requires localization
L["APX_ABOUT_MINIMAP_DESC"] = "This will remove the button from the minimap." -- Requires localization
L["APX_ABOUT_MINIMAP_ICON"] = "Minimap Icon" -- Requires localization
L["APX_ABOUT_VERSION"] = "|cffffd700Version :|r |cff20ff20 %s|r" -- Requires localization
L["APX_ABOUT_WEBSITE"] = "|cffffd700Website :|r |cff00ccff%s|r" -- Requires localization
L["APX_ARMOR_CLOTH"] = "Cloth" -- Requires localization
L["APX_ARMOR_LEATHER"] = "Leather" -- Requires localization
L["APX_ARMOR_MAIL"] = "Mail" -- Requires localization
L["APX_ARMOR_NONE"] = "None" -- Requires localization
L["APX_ARMOR_PALTE"] = "Plate" -- Requires localization
L["APX_BANNER_FORMAT"] = "version |cff2f8fa7%s|r Loaded, using |cff2f8fa7%s|r of memory." -- Requires localization
L["APX_BUTTON_NOJUNK"] = "You have no junk items in your inventory." -- Requires localization
L["APX_BUTTON_PRINT_PROFIT"] = "Total profits: %s" -- Requires localization
L["APX_BUTTON_SELLJUNK"] = "Sell Junk Items" -- Requires localization
L["APX_BUTTON_SETTINGS"] = "Button Settings" -- Requires localization
L["APX_COIN"] = "Coin" -- Requires localization
L["APX_COLORED"] = "Colored" -- Requires localization
L["APX_COLORLESS"] = "Colorless" -- Requires localization
L["APX_CONCENT_SOULBOUND_WARNING"] = [=[
     |cffff0000WARNING     WARNING     WARNING|r
     |cffff0000===============================|r

     This function will |cffff0000SELL ANY SOULBOUND|r item that you have in your inventory that are not
     part of an |cff00ff00Equipment Set|r or that is |cff00ff00Class Gear|r

     |cffff0000ALSO NOTE :|r You may only buy back 12 items from the vendor so if |cff00ff00"Auto Sell"|r
     is on and you have |cffc0c0c0JUNK ITEMS|r in your inventory you |cffff0000MAY NOT|r be able to buy your gear back.

     |cffff0000YOU WOULD NEED TO OPEN A TICKET TO GET YOUR GEAR BACK.|r

     If you click |cffff0000"Accept"|r then you accept all
     responsibility for any gear that this AddOn may sell
]=] -- Requires localization
L["APX_COPPER_1_COIN"] = "|cffffffff%d|r|TInterface\\MoneyFrame\\UI-CopperIcon:%d:%d:2:0|t" -- Requires localization
L["APX_COPPER_1_TEXT"] = "|cffffffff%dc|r" -- Requires localization
L["APX_COPPER_2_COIN"] = "|cffb87333%d|r|TInterface\\MoneyFrame\\UI-CopperIcon:%d:%d:2:0|t" -- Requires localization
L["APX_COPPER_2_TEXT"] = "|cffb87333%dc|r" -- Requires localization
L["APX_DEBUG_DESC"] = "You should only use this when you are having errors to see if this addon is causing the problem." -- Requires localization
L["APX_DEBUG_LABEL"] = "Debuging" -- Requires localization
L["APX_DEBUG_OPTDEBUG"] = "Debug" -- Requires localization
L["APX_DEBUG_OPTDEBUG_DESC"] = "This will turn on internal debugging" -- Requires localization
L["APX_DEBUG_SETTINGS"] = "Debug Setting" -- Requires localization
L["APX_FORMATED_ADD"] = "Added |cff2f8fa7%s|r to |cff2f8fa7%s|r list" -- Requires localization
L["APX_FORMATED_COIN"] = "Sold %s for %s" -- Requires localization
L["APX_FORMATED_COINSTACK"] = "Sold %s x%s for %s" -- Requires localization
L["APX_FORMATED_ICA"] = "Classes: %s" -- Requires localization
L["APX_FORMATED_IES"] = "Equipment Sets:" -- Requires localization
L["APX_FORMATED_LM"] = "|c%x+|Hitem:[%-%d:]*|h%[.-%]|h|r" -- Requires localization
L["APX_FORMATED_PROFITS"] = "Total Profits : %s" -- Requires localization
L["APX_FORMATED_REMOVE"] = "Removed |cff2f8fa7%s|r from |cff2f8fa7%s|r list" -- Requires localization
L["APX_FORMATED_TEXT"] = "Sold %s." -- Requires localization
L["APX_FORMATED_TEXTSTACK"] = "Sold %s x%s." -- Requires localization
L["APX_FRAME_ADD"] = "Add Item %s : " -- Requires localization
L["APX_FRAME_EXCEPTIONS"] = "Exceptions" -- Requires localization
L["APX_FRAME_EXCEPTIONS_DESC"] = [=[

     Anything items placed in this list will be |cffff0000SOLD|r without question.
     
     This is for item that commonly drop that you no longer have a user for.

     |cffff0000WARNING :|r Be carefull as to what you place in this list, |cffff0000IT WILL BE SOLD|r.
]=] -- Requires localization
L["APX_FRAME_EXEMPTIONS"] = "Exemptions" -- Requires localization
L["APX_FRAME_EXEMPTIONS_DESC"] = [=[

     Anything items placed in this list will |cff00ff00NOT|r be sold no matter when.

     Have a fun gray item that you do nto want to be sold?
     
     Here is the place to put it.
]=] -- Requires localization
L["APX_FRAME_REMOVE"] = "Remove Item %s : " -- Requires localization
L["APX_GOLD_1_COIN"] = "|cffffffff%d|r|TInterface\\MoneyFrame\\UI-GoldIcon:%d:%d:2:0|t" -- Requires localization
L["APX_GOLD_1_TEXT"] = "|cffffffff%dg|r" -- Requires localization
L["APX_GOLD_2_COIN"] = "|cffffd700%d|r|TInterface\\MoneyFrame\\UI-GoldIcon:%d:%d:2:0|t" -- Requires localization
L["APX_GOLD_2_TEXT"] = "|cffffd700%dg|r" -- Requires localization
L["APX_MINIMAP_TEXT1"] = "|cff2f8fa7AutoProfitX|r |cffffffff- %s|r" -- Requires localization
L["APX_MINIMAP_TEXT2"] = "|cff2f8fa7Click|r|cffffffff to open exceptions tool.|r" -- Requires localization
L["APX_MINIMAP_TEXT3"] = "|cff2f8fa7Right-click|r|cffffffff to open the options menu|r" -- Requires localization
L["APX_MINIMAP_TEXT4"] = "|cff2f8fa7Invintory Junk : |r|cffffffff%s|r" -- Requires localization
L["APX_MISC_MOUSEOVER"] = "Mouse-over" -- Requires localization
L["APX_MISC_MOUSEOVER_PROFIT"] = "Mouse-over and profit" -- Requires localization
L["APX_MISC_NEVER"] = "Never spin" -- Requires localization
L["APX_MISC_PROFIT"] = "Profits" -- Requires localization
L["APX_OPTIONS_ADDON_LABEL"] = "AutoProfitX" -- Requires localization
L["APX_OPTIONS_AUTOSELL"] = "Auto Sell" -- Requires localization
L["APX_OPTIONS_AUTOSELL_DESC"] = "Automatically sell junk items when opening vendor window." -- Requires localization
L["APX_OPTIONS_BADLINK"] = "Invalid item link provided." -- Requires localization
L["APX_OPTIONS_BAG"] = "Open Bags" -- Requires localization
L["APX_OPTIONS_BAG_DESC"] = "This will open all bags when you visit a vendor." -- Requires localization
L["APX_OPTIONS_BUTTONA"] = "Button Animation Options" -- Requires localization
L["APX_OPTIONS_BUTTONA_DESC"] = "Set up when you want the treasure pile in the button to spin." -- Requires localization
L["APX_OPTIONS_COLOR_TEXT"] = "Colored Text" -- Requires localization
L["APX_OPTIONS_COLOR_TEXT_DESC"] = "Coloring setting for coin / normal format." -- Requires localization
L["APX_OPTIONS_FANCY"] = "Item Value" -- Requires localization
L["APX_OPTIONS_FANCY_DESC"] = "This will show item value in sales report, Per Item." -- Requires localization
L["APX_OPTIONS_FANCY_PROFIT"] = "Show Coins" -- Requires localization
L["APX_OPTIONS_FANCY_PROFIT_DESC"] = "Print total profit after sale using coin format." -- Requires localization
L["APX_OPTIONS_GOLDPILE"] = "Gold Pile Spin Rate" -- Requires localization
L["APX_OPTIONS_GOLDPILE_DESC"] = "This will change the speed at witch the pile of gold spins if you have stuff to sell." -- Requires localization
L["APX_OPTIONS_LABEL"] = "Options" -- Requires localization
L["APX_OPTIONS_LABEL_ADDON"] = "AddOn" -- Requires localization
L["APX_OPTIONS_PRINT_LABEL"] = "Print Settings" -- Requires localization
L["APX_OPTIONS_PROFIT"] = "Show Profit" -- Requires localization
L["APX_OPTIONS_PROFIT_DESC"] = "Print total profit after sale." -- Requires localization
L["APX_OPTIONS_SALES"] = "Sales Reports" -- Requires localization
L["APX_OPTIONS_SALES_DESC"] = "Print items being sold in chat frame." -- Requires localization
L["APX_OPTIONS_SELLARMORSUB"] = "Sell Armor Sub Class" -- Requires localization
L["APX_OPTIONS_SELLARMORSUB_DESC"] = [=[
|cffff9900The Following list of sub armors classes will be considered unusable with this option enabled, After Level 40.|r
%s
Tooltip info changes per class.
]=] -- Requires localization
L["APX_OPTIONS_SETTINGS_LABEL"] = "Settings" -- Requires localization
L["APX_OPTIONS_SOULBOUND"] = "Sell Soulbound" -- Requires localization
L["APX_OPTIONS_SOULBOUND_DESC"] = [=[
     Sell soulbound items.

|cffff0000WARNING:|r This |cffff0000SELLS ALL|r soulbound items that are in your invintory, that are not part of a Equipment set.
]=] -- Requires localization
L["APX_OPTIONS_STACK"] = "Stack Count" -- Requires localization
L["APX_OPTIONS_STACK_DESC"] = "This will show the stack count int he sales report." -- Requires localization
L["APX_PRINT_DEBUGLINE"] = "|cff2f8fa7AutoProfitX Debug|r %s" -- Requires localization
L["APX_PRINT_ERRORLINE"] = "|cff2f8fa7AutoProfitX Error|r %s" -- Requires localization
L["APX_PRINT_PRINTLINE"] = "|cff2f8fa7AutoProfitX|r %s" -- Requires localization
L["APX_RESET_DB"] = [=[
%s

Your Version : |cff00ff00%s|r
Current Version : |cffff0000%s|r

Your Data Array Has Been Reset!

]=] -- Requires localization
L["APX_SILVER_1_COIN"] = "|cffffffff%d|r|TInterface\\MoneyFrame\\UI-SilverIcon:%d:%d:2:0|t" -- Requires localization
L["APX_SILVER_1_TEXT"] = "|cffffffff%ds|r" -- Requires localization
L["APX_SILVER_2_COIN"] = "|cffc0c0c0%d|r|TInterface\\MoneyFrame\\UI-SilverIcon:%d:%d:2:0|t" -- Requires localization
L["APX_SILVER_2_TEXT"] = "|cffc0c0c0%ds|r" -- Requires localization
L["APX_SLASH_ADD"] = "add" -- Requires localization
L["APX_SLASH_ADD_DESC"] = "Usage : %s <item link>[<item link>...]" -- Requires localization
L["APX_SLASH_ADD_EXCEPTIONS"] = "addExceptions" -- Requires localization
L["APX_SLASH_ADD_EXEMPTIONS"] = "addExemptions" -- Requires localization
L["APX_SLASH_LABEL"] = "AutoProfitX Command Handler" -- Requires localization
L["APX_SLASH_LIST"] = "List" -- Requires localization
L["APX_SLASH_LIST_DESC"] = "Usage : List [ %s / %s ]" -- Requires localization
L["APX_SLASH_OPTIONS"] = "Options" -- Requires localization
L["APX_SLASH_OPTIONS_DESC"] = "Show options panel." -- Requires localization
L["APX_SLASH_REMOVE"] = "remove" -- Requires localization
L["APX_SLASH_REMOVE_DESC"] = "Usage : %s <item link>[<item link>...]" -- Requires localization
L["APX_SLASH_REMOVE_EXCEPTIONS"] = "removeExceptions" -- Requires localization
L["APX_SLASH_REMOVE_EXEMPTIONS"] = "removeExemptions" -- Requires localization
L["APX_SLASH_TOOLS"] = "Tools" -- Requires localization
L["APX_SLASH_TOOLS_DESC"] = "%s / %s %s." -- Requires localization
L["APX_TEXT"] = "Text" -- Requires localization