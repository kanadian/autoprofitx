﻿local L = LibStub("AceLocale-3.0"):NewLocale("AutoProfitX", "zhTW");
if ( not L ) then return; end 

L["APX_ABOUT_ABOUT"] = "關於"
L["APX_ABOUT_ABOUT_DESC"] = "當你在商人視窗點擊按鈕時自動販售灰色物品。"
L["APX_ABOUT_AUTHOR"] = "|cffffd700作者 :|r |cffff8c00 %s|r"
L["APX_ABOUT_CATEGORY"] = "|cffffd700類別 :|r |cffff8c00 %s|r"
L["APX_ABOUT_EMAIL"] = "|cffffd700Email :|r |cffff8c00 %s|r"
L["APX_ABOUT_LDB_DESC"] = [=[|cffc0c0c0這小地圖按鈕是一個LDB圖示之類，你可以禁用小地圖按鈕但請啟用面版的圖示

     示例：這將增加AutoProfitX按鈕在APX面板|r]=]
L["APX_ABOUT_LOAD"] = "插件載入訊息"
L["APX_ABOUT_LOAD_DESC"] = "當插件已載入時將在聊天框顯示一條訊息。"
L["APX_ABOUT_MEMORY"] = "記憶體使用量"
L["APX_ABOUT_MEMORY_DESC"] = "這將會在聊天視窗裡顯示記憶體使用量。"
L["APX_ABOUT_MINIMAP"] = "隱藏小地圖按鈕"
L["APX_ABOUT_MINIMAP_DESC"] = "這將會從小地圖移除按鈕。"
L["APX_ABOUT_MINIMAP_ICON"] = "小地圖圖示"
L["APX_ABOUT_VERSION"] = "|cffffd700版本 :|r |cff20ff20 %s|r"
L["APX_ABOUT_WEBSITE"] = "|cffffd700網址 :|r |cff00ccff%s|r"
L["APX_ARMOR_CLOTH"] = "布甲"
L["APX_ARMOR_LEATHER"] = "皮甲"
L["APX_ARMOR_MAIL"] = "鎖甲"
L["APX_ARMOR_NONE"] = "無"
L["APX_ARMOR_PALTE"] = "鎧甲"
L["APX_BANNER_FORMAT"] = "version |cff2f8fa7%s|r Loaded, using |cff2f8fa7%s|r of memory." -- Requires localization
L["APX_BUTTON_NOJUNK"] = "你的背包內沒有灰色物品。"
L["APX_BUTTON_PRINT_PROFIT"] = "總售價: %s"
L["APX_BUTTON_SELLJUNK"] = "出售灰色物品"
L["APX_BUTTON_SETTINGS"] = "按鈕設置"
L["APX_COIN"] = "Coin" -- Requires localization
L["APX_COLORED"] = "Colored" -- Requires localization
L["APX_COLORLESS"] = "Colorless" -- Requires localization
L["APX_CONCENT_SOULBOUND_WARNING"] = [=[
     |cffff0000WARNING     WARNING     WARNING|r
     |cffff0000===============================|r

     This function will |cffff0000SELL ANY SOULBOUND|r item that you have in your inventory that are not
     part of an |cff00ff00Equipment Set|r or that is |cff00ff00Class Gear|r

     |cffff0000ALSO NOTE :|r You may only buy back 12 items from the vendor so if |cff00ff00"Auto Sell"|r
     is on and you have |cffc0c0c0JUNK ITEMS|r in your inventory you |cffff0000MAY NOT|r be able to buy your gear back.

     |cffff0000YOU WOULD NEED TO OPEN A TICKET TO GET YOUR GEAR BACK.|r

     If you click |cffff0000"Accept"|r then you accept all
     responsibility for any gear that this AddOn may sell
]=] -- Requires localization
L["APX_COPPER_1_COIN"] = "|cffffffff%d|r|TInterface\\MoneyFrame\\UI-CopperIcon:%d:%d:2:0|t" -- Requires localization
L["APX_COPPER_1_TEXT"] = "|cffffffff%dc|r" -- Requires localization
L["APX_COPPER_2_COIN"] = "|cffb87333%d|r|TInterface\\MoneyFrame\\UI-CopperIcon:%d:%d:2:0|t" -- Requires localization
L["APX_COPPER_2_TEXT"] = "|cffb87333%dc|r" -- Requires localization
L["APX_DEBUG_DESC"] = "你應該只使用在當您遇到錯誤並想看看什麼問題導致。"
L["APX_DEBUG_LABEL"] = "偵錯中"
L["APX_DEBUG_OPTDEBUG"] = "偵錯"
L["APX_DEBUG_OPTDEBUG_DESC"] = "這將打開內部偵錯"
L["APX_DEBUG_SETTINGS"] = "偵錯設置"
L["APX_FORMATED_ADD"] = "已新增 |cff2f8fa7%s|r 到 |cff2f8fa7%s|r 清單"
L["APX_FORMATED_COIN"] = "售出 %s 給 %s"
L["APX_FORMATED_COINSTACK"] = "售出 %s x%s 給 %s"
L["APX_FORMATED_ICA"] = "職業: %s"
L["APX_FORMATED_IES"] = "換裝套件："
L["APX_FORMATED_LM"] = "|c%x+|Hitem:[%-%d:]*|h%[.-%]|h|r" -- Requires localization
L["APX_FORMATED_PROFITS"] = "總售價 : %s"
L["APX_FORMATED_REMOVE"] = "已移除|cff2f8fa7%s|r從|cff2f8fa7%s|r 清單"
L["APX_FORMATED_TEXT"] = "售出 %s。"
L["APX_FORMATED_TEXTSTACK"] = "售出 %s x%s。"
L["APX_FRAME_ADD"] = "新增物品 %s :"
L["APX_FRAME_EXCEPTIONS"] = "例外"
L["APX_FRAME_EXCEPTIONS_DESC"] = [=[任何在此陳列清單的物品將會 |cffff0000售出|r無任何確認。
     
This is for item that commonly drop that you no longer have a user for.

|cffff0000警告：|r小心你放在此清單的物品，|cffff0000他會被出售|r。]=]
L["APX_FRAME_EXEMPTIONS"] = "豁免"
L["APX_FRAME_EXEMPTIONS_DESC"] = [=[任何在此陳列清單的物品無論何時將|cff00ff00不會|r被出售。

一個有趣的灰色物品，你不想出售？
     
這是擺放它的地方。]=]
L["APX_FRAME_REMOVE"] = "移除物品 %s :"
L["APX_GOLD_1_COIN"] = "|cffffffff%d|r|TInterface\\MoneyFrame\\UI-GoldIcon:%d:%d:2:0|t" -- Requires localization
L["APX_GOLD_1_TEXT"] = "|cffffffff%dg|r" -- Requires localization
L["APX_GOLD_2_COIN"] = "|cffffd700%d|r|TInterface\\MoneyFrame\\UI-GoldIcon:%d:%d:2:0|t" -- Requires localization
L["APX_GOLD_2_TEXT"] = "|cffffd700%dg|r" -- Requires localization
L["APX_MINIMAP_TEXT1"] = "|cff2f8fa7AutoProfitX|r |cffffffff- %s|r"
L["APX_MINIMAP_TEXT2"] = "|cff2f8fa7點擊|r|cffffffff以打開例外工具。|r"
L["APX_MINIMAP_TEXT3"] = "|cff2f8fa7右鍵點擊|r|cffffffff以打開選項。|r"
L["APX_MINIMAP_TEXT4"] = "|cff2f8fa7Invintory Junk : |r|cffffffff%s|r" -- Requires localization
L["APX_MISC_MOUSEOVER"] = "滑鼠經過"
L["APX_MISC_MOUSEOVER_PROFIT"] = "滑鼠經過或售出物品時"
L["APX_MISC_NEVER"] = "不轉動"
L["APX_MISC_PROFIT"] = "售出物品時"
L["APX_OPTIONS_ADDON_LABEL"] = "AutoProfitX"
L["APX_OPTIONS_AUTOSELL"] = "自動售出"
L["APX_OPTIONS_AUTOSELL_DESC"] = "當打開商人視窗時，自動售出灰色物品。"
L["APX_OPTIONS_BADLINK"] = "無效的物品鏈結。"
L["APX_OPTIONS_BAG"] = "開啟背包"
L["APX_OPTIONS_BAG_DESC"] = "當你拜訪商人時，將打開所有的背包。"
L["APX_OPTIONS_BUTTONA"] = "按鈕動畫選項"
L["APX_OPTIONS_BUTTONA_DESC"] = "Set up when you want the treasure pile in the button to spin." -- Requires localization
L["APX_OPTIONS_COLOR_TEXT"] = "幣別著色"
L["APX_OPTIONS_COLOR_TEXT_DESC"] = "各種幣別的著色設定 / 一般格式。"
L["APX_OPTIONS_FANCY"] = "物品價格"
L["APX_OPTIONS_FANCY_DESC"] = "這會在販售報告中顯示每件物品的價格。"
L["APX_OPTIONS_FANCY_PROFIT"] = "顯示幣別圖示"
L["APX_OPTIONS_FANCY_PROFIT_DESC"] = "在販賣後使用幣別圖示的格式顯示總獲利。"
L["APX_OPTIONS_GOLDPILE"] = "金幣堆旋轉速度"
L["APX_OPTIONS_GOLDPILE_DESC"] = "如果你有東西要出售，這將會改變金幣堆的旋轉速度。"
L["APX_OPTIONS_LABEL"] = "選項"
L["APX_OPTIONS_LABEL_ADDON"] = "插件"
L["APX_OPTIONS_PRINT_LABEL"] = "顯示設定"
L["APX_OPTIONS_PROFIT"] = "顯示獲利"
L["APX_OPTIONS_PROFIT_DESC"] = "在售出後顯示總獲利。"
L["APX_OPTIONS_SALES"] = "販售報告"
L["APX_OPTIONS_SALES_DESC"] = "在聊天視窗顯示販售的物品。"
L["APX_OPTIONS_SELLARMORSUB"] = "販賣護甲類型"
L["APX_OPTIONS_SELLARMORSUB_DESC"] = [=[|cffff9900此選項啟用時，下面列表的護甲類型將被視為無法使用，在40級以後。|r
%s
提示訊息將依各職業而改變。]=]
L["APX_OPTIONS_SETTINGS_LABEL"] = "設定"
L["APX_OPTIONS_SOULBOUND"] = "販賣靈魂榜定物品"
L["APX_OPTIONS_SOULBOUND_DESC"] = [=[    販賣靈魂榜定物品。

|cffff0000警告：|r這會 |cffff0000販賣所有|r 在您背包內的靈魂榜定物品，不包含換裝套件中的物品。]=]
L["APX_OPTIONS_STACK"] = "包括堆疊數"
L["APX_OPTIONS_STACK_DESC"] = "這將在販售報告顯示堆疊數。"
L["APX_PRINT_DEBUGLINE"] = "|cff2f8fa7AutoProfitX 偵錯|r %s"
L["APX_PRINT_ERRORLINE"] = "|cff2f8fa7AutoProfitX 錯誤|r %s"
L["APX_PRINT_PRINTLINE"] = "|cff2f8fa7AutoProfitX|r %s"
L["APX_RESET_DB"] = [=[%s

你的版本 : |cff00ff00%s|r
當前版本 : |cffff0000%s|r

你的數據陣列已被重置!

]=]
L["APX_SILVER_1_COIN"] = "|cffffffff%d|r|TInterface\\MoneyFrame\\UI-SilverIcon:%d:%d:2:0|t" -- Requires localization
L["APX_SILVER_1_TEXT"] = "|cffffffff%ds|r" -- Requires localization
L["APX_SILVER_2_COIN"] = "|cffc0c0c0%d|r|TInterface\\MoneyFrame\\UI-SilverIcon:%d:%d:2:0|t" -- Requires localization
L["APX_SILVER_2_TEXT"] = "|cffc0c0c0%ds|r" -- Requires localization
L["APX_SLASH_ADD"] = "新增"
L["APX_SLASH_ADD_DESC"] = "用法 : %s <物品連結>[<物品連結>...]"
L["APX_SLASH_ADD_EXCEPTIONS"] = "addExceptions" -- Requires localization
L["APX_SLASH_ADD_EXEMPTIONS"] = "addExemptions" -- Requires localization
L["APX_SLASH_LABEL"] = "AutoProfitX 命令控制"
L["APX_SLASH_LIST"] = "列出"
L["APX_SLASH_LIST_DESC"] = "用法 : 列出 [ %s / %s ]"
L["APX_SLASH_OPTIONS"] = "選項"
L["APX_SLASH_OPTIONS_DESC"] = "顯示選項面板。"
L["APX_SLASH_REMOVE"] = "移除"
L["APX_SLASH_REMOVE_DESC"] = "用法 : %s <物品連結>[<物品連結>...]"
L["APX_SLASH_REMOVE_EXCEPTIONS"] = "removeExceptions" -- Requires localization
L["APX_SLASH_REMOVE_EXEMPTIONS"] = "removeExemptions" -- Requires localization
L["APX_SLASH_TOOLS"] = "Tools" -- Requires localization
L["APX_SLASH_TOOLS_DESC"] = "%s / %s %s." -- Requires localization
L["APX_TEXT"] = "Text" -- Requires localization