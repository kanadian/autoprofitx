local L = LibStub( "AceLocale-3.0" ):NewLocale( "AutoProfitX" , "frFR" );
if ( not L ) then return; end

L["APX_MISC_NEVER"] = "Never spin"
L["APX_MISC_MOUSEOVER_PROFIT"] = "Mouse-over and profit"
L["APX_MISC_MOUSEOVER"] = "Mouse-over"
L["APX_MISC_PROFIT"] = "Profits"
L["APX_OPTIONS_ADDON_LABEL"] = "AutoProfitX"
L["APX_OPTIONS_LABEL_ADDON"] = "AddOn"
L["APX_OPTIONS_SETTINGS_LABEL"] = "Settings"
L["APX_OPTIONS_BADLINK"] = "Invalid item link provided."
L["APX_OPTIONS_AUTOSELL"] = "Auto Sell"
L["APX_OPTIONS_AUTOSELL_DESC"] = "Automatically sell junk items when opening vendor window."
L["APX_OPTIONS_BAG"] = "Open Bags"
L["APX_OPTIONS_BAG_DESC"] = "This will open all bags when you visit a vendor."
L["APX_OPTIONS_SOULBOUND"] = "Sell Soulbound"
L["APX_OPTIONS_SOULBOUND_DESC"] = [=[
     Sell soulbound items.

|cffff0000WARNING:|r This |cffff0000SELLS ALL|r soulbound items that are in your invintory, that are not part of a Equipment set.
]=]
L["APX_OPTIONS_SELLARMORSUB"] = "Sell Armor Sub Class"
L["APX_OPTIONS_SELLARMORSUB_DESC"] = [=[
|cffff9900The Following list of sub armors classes will be considered unusable with this option enabled, After Level 40.|r
%s
Tooltip info changes per class.
]=]
L["APX_OPTIONS_PRINT_LABEL"] = "Print Settings"
L["APX_OPTIONS_PROFIT"] = "Show Profit"
L["APX_OPTIONS_PROFIT_DESC"] = "Print total profit after sale."
L["APX_OPTIONS_SALES"] = "Sales Reports"
L["APX_OPTIONS_SALES_DESC"] = "Print items being sold in chat frame."
L["APX_OPTIONS_FANCY"] = "Item Value"
L["APX_OPTIONS_FANCY_DESC"] = "This will show item value in sales report, Per Item."
L["APX_OPTIONS_STACK"] = "Stack Count"
L["APX_OPTIONS_STACK_DESC"] = "This will show the stack count int he sales report."
L["APX_OPTIONS_FANCY_PROFIT"] = "Show Coins"
L["APX_OPTIONS_FANCY_PROFIT_DESC"] = "Print total profit after sale using coin format."
L["APX_OPTIONS_COLOR_TEXT"] = "Colored Text"
L["APX_OPTIONS_COLOR_TEXT_DESC"] = "Coloring setting for coin / normal format."
L["APX_OPTIONS_GOLDPILE"] = "Gold Pile Spin Rate"
L["APX_OPTIONS_GOLDPILE_DESC"] = "This will change the speed at witch the pile of gold spins if you have stuff to sell."
L["APX_OPTIONS_BUTTONA"] = "Button Animation Options"
L["APX_OPTIONS_BUTTONA_DESC"] = "Set up when you want the treasure pile in the button to spin."
L["APX_OPTIONS_LABEL"] = "Options"
L["APX_OPTIONS_TEXT_G"] = "%dg"
L["APX_OPTIONS_TEXT_S"] = "%ds"
L["APX_OPTIONS_TEXT_C"] = "%dc"
L["APX_OPTIONS_TEXT_COLOR_G"] = "|cffffd700%dg|r"
L["APX_OPTIONS_TEXT_COLOR_S"] = "|cffc0c0c0%ds|r"
L["APX_OPTIONS_TEXT_COLOR_C"] = "|cffb87333%dc|r"
L["APX_GOLD_COIN"] = "%d|TInterface\\MoneyFrame\\UI-GoldIcon:%d:%d:2:0|t"
L["APX_GOLD_COLOR_COIN"] = "|cffffd700%d|r|TInterface\\MoneyFrame\\UI-GoldIcon:%d:%d:2:0|t"
L["APX_SILVER_COIN"] = "%d|TInterface\\MoneyFrame\\UI-SilverIcon:%d:%d:2:0|t"
L["APX_SILVER_COLOR_COIN"] = "|cffc0c0c0%d|r|TInterface\\MoneyFrame\\UI-SilverIcon:%d:%d:2:0|t"
L["APX_COPPER_COIN"] = "%d|TInterface\\MoneyFrame\\UI-CopperIcon:%d:%d:2:0|t"
L["APX_COPPER_COLOR_COIN"] = "|cffb87333%d|r|TInterface\\MoneyFrame\\UI-CopperIcon:%d:%d:2:0|t"
L["APX_BUTTON_SETTINGS"] = "Button Settings"
L["APX_BUTTON_SELLJUNK"] = "Sell Junk Items"
L["APX_BUTTON_NOJUNK"] = "You have no junk items in your inventory."
L["APX_BUTTON_PRINT_PROFIT"] = "Total profits: %s"
L["APX_ABOUT_ABOUT"] = "About"
L["APX_ABOUT_ABOUT_DESC"] = "Automatically sell junk items when you click the button on the vendors window."
L["APX_ABOUT_VERSION"] = "|cffffd700Version :|r |cff20ff20 %s|r"
L["APX_ABOUT_AUTHOR"] = "|cffffd700Author :|r |cffff8c00 %s|r"
L["APX_ABOUT_CATEGORY"] = "|cffffd700Category :|r |cffff8c00 %s|r"
L["APX_ABOUT_EMAIL"] = "|cffffd700Email :|r |cffff8c00 %s|r"
L["APX_ABOUT_WEBSITE"] = "|cffffd700Website :|r |cff00ccff%s|r"
L["APX_ABOUT_LOAD"] = "Addon Load Message"
L["APX_ABOUT_LOAD_DESC"] = "This will display a message in the chat frame when then addon is fully loaded."
L["APX_ABOUT_MEMORY"] = "Memory Usage"
L["APX_ABOUT_MEMORY_DESC"] = "This will display memory usgae in the chat frame."
L["APX_ABOUT_MINIMAP_ICON"] = "Minimap Icon"
L["APX_ABOUT_MINIMAP"] = "Hide Minimap Button"
L["APX_ABOUT_MINIMAP_DESC"] = "This will remove the button from the minimap."
L["APX_ABOUT_MEMORY_FORMATED"] = "is using|cff2f8fa7 %s kb|r of memory."
L["APX_ABOUT_LDB_DESC"] = [=[ 
|cffc0c0c0The Minimap icon is a LDBIcon stream, You can disable the Minimap icon but enable an Infobar Button.

     Example : This will Add an AutoProfitX button to APX Panel|r
]=]
L["APX_FRAME_EXCEPTIONS"] = "Exceptions"
L["APX_FRAME_EXCEPTIONS_DESC"] = [=[

     Anything items placed in this list will be |cffff0000SOLD|r without question.
     
     This is for item that commonly drop that you no longer have a user for.

     |cffff0000WARNING :|r Be carefull as to what you place in this list, |cffff0000IT WILL BE SOLD|r.
]=]
L["APX_FRAME_EXEMPTIONS"] = "Exemptions"
L["APX_FRAME_EXEMPTIONS_DESC"] = [=[

     Anything items placed in this list will |cff00ff00NOT|r be sold no matter when.

     Have a fun gray item that you do nto want to be sold?
     
     Here is the place to put it.
]=]
L["APX_FRAME_ADD"] = "Add Item %s : "
L["APX_FRAME_REMOVE"] = "Remove Item %s : "
L["APX_SLASH_LABEL"] = "AutoProfitX Command Handler"
L["APX_SLASH_OPTIONS"] = "Options"
L["APX_SLASH_OPTIONS_DESC"] = "Show options panel."
L["APX_SLASH_TOOLS"] = "Tools"
L["APX_SLASH_TOOLS_DESC"] = "%s / %s %s."
L["APX_SLASH_LIST"] = "List"
L["APX_SLASH_LIST_DESC"] = "Usage : List [ %s / %s ]"
L["APX_SLASH_ADD_EXCEPTIONS"] = "addExceptions"
L["APX_SLASH_ADD"] = "add"
L["APX_SLASH_ADD_DESC"] = "Usage : %s <item link>[<item link>...]"
L["APX_SLASH_REMOVE_EXCEPTIONS"] = "removeExceptions"
L["APX_SLASH_REMOVE"] = "remove"
L["APX_SLASH_REMOVE_DESC"] = "Usage : %s <item link>[<item link>...]"
L["APX_SLASH_ADD_EXEMPTIONS"] = "addExemptions"
L["APX_SLASH_REMOVE_EXEMPTIONS"] = "removeExemptions"
L["APX_DEBUG_LABEL"] = "Debuging"
L["APX_DEBUG_OPTDEBUG"] = "Debug"
L["APX_DEBUG_OPTDEBUG_DESC"] = "This will turn on internal debugging"
L["APX_DEBUG_SETTINGS"] = "Debug Setting"
L["APX_DEBUG_DESC"] = "You should only use this when you are having errors to see if this addon is causing the problem."
L["APX_FORMATED_LM"] = "|c%x+|Hitem:[%-%d:]*|h%[.-%]|h|r"
L["APX_FORMATED_IES"] = "Equipment Sets:"
L["APX_FORMATED_ICA"] = "Classes: %s"
L["APX_FORMATED_TEXT"] = "Sold %s."
L["APX_FORMATED_COIN"] = "Sold %s for %s"
L["APX_FORMATED_COINSTACK"] = "Sold %s x%s for %s"
L["APX_FORMATED_TEXTSTACK"] = "Sold %s x%s."
L["APX_FORMATED_PROFITS"] = "Total Profits : %s"
L["APX_FORMATED_REMOVE"] = "Removed |cff2f8fa7%s|r from |cff2f8fa7%s|r list"
L["APX_FORMATED_ADD"] = "Added |cff2f8fa7%s|r to |cff2f8fa7%s|r list"
L["APX_MINIMAP_TEXT1"] = "|cff2f8fa7AutoProfitX|r |cffffffff- %s|r"
L["APX_MINIMAP_TEXT2"] = "|cff2f8fa7Click|r|cffffffff to open exceptions tool.|r"
L["APX_MINIMAP_TEXT3"] = "|cff2f8fa7Right-click|r|cffffffff to open the options menu|r"
L["APX_PRINT_DEBUGLINE"] = "|cff2f8fa7AutoProfitX Debug|r %s"
L["APX_PRINT_PRINTLINE"] = "|cff2f8fa7AutoProfitX|r %s"
L["APX_PRINT_ERRORLINE"] = "|cff2f8fa7AutoProfitX Error|r %s"
L["APX_ARMOR_LEATHER"] = "Leather"
L["APX_ARMOR_MAIL"] = "Mail"
L["APX_ARMOR_PALTE"] = "Plate"
L["APX_ARMOR_CLOTH"] = "Cloth"
L["APX_ARMOR_NONE"] = "None"
L["APX_CONCENT_SOULBOUND_WARNING"] = [=[
     |cffff0000WARNING     WARNING     WARNING|r
     |cffff0000===============================|r

     This function will |cffff0000SELL ANY SOULBOUND|r item that you have in your inventory that are not
     part of an |cff00ff00Equipment Set|r or that is |cff00ff00Class Gear|r

     |cffff0000ALSO NOTE :|r You may only buy back 12 items from the vendor so if |cff00ff00"Auto Sell"|r
     is on and you have |cffc0c0c0JUNK ITEMS|r in your inventory you |cffff0000MAY NOT|r be able to buy your gear back.

     |cffff0000YOU WOULD NEED TO OPEN A TICKET TO GET YOUR GEAR BACK.|r

     If you click |cffff0000"Accept"|r then you accept all
     responsibility for any gear that this AddOn may sell
]=]
L["APX_RESET_DB"] = [=[
%s

Your Version : |cff00ff00%s|r
Current Version : |cffff0000%s|r

Your Data Array Has Been Reset!

]=]