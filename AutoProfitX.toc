﻿## Interface: 100002
## Title: AutoProfitX
## Notes: Automatically sells low quality gray items and selected white items to the vendor.
## Author: KanadiaN
## Version: r96
## SavedVariables: AutoProfitXDB
## OptionalDeps: Ace3
## X-Credits: BigZero ( original author )
## X-Compatible-With: 100000
## X-Category: Inventory
## X-eMail: bug@kanadian.com
## X-Notes: Based on BigZero's AutoProfitX v2.04.
## X-WebSite: http://www.wowace.com/addons/autoprofitx/

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

Locals\Locals.xml

AutoProfitX-BlizzTable.lua
AutoProfitX-Config.lua
AutoProfitX-Prof.lua
AutoProfitX.xml